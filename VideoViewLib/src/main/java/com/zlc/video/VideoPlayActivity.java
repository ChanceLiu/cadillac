package com.zlc.video;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.zlc.video.ui.BaseActivity;
import com.zlc.video.utils.ScreenUtil;
import com.zlc.video.utils.StatusBarUtil;

public class VideoPlayActivity extends BaseActivity {

    private RelativeLayout rl_video_container;
    private VideoView videoview;
    private VideoController id_video_controller;
    private VideoBusiness mVideoBusiness;
    private String mVideoUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// 隐藏标题
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);// 设置全屏
        setContentView(getLayoutId());
        mVideoUrl = getIntent().getStringExtra("path");
        initView();
        initData();
        initListener();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_video_play;
    }

    @Override
    protected void initView() {
        super.initView();
        rl_video_container = findView(R.id.rl_video_container);
        videoview = findView(R.id.id_videoview);
        id_video_controller = findView(R.id.id_video_controller);
    }

    @Override
    protected void initData() {
        super.initData();
        setVideoInfo();
    }

    private void setVideoInfo() {
        //放一个视频到res/raw下就能播放 或者放一个在线地址
//        Uri uri= Uri.parse("android.resource://" + getPackageName() + "/"/*+R.raw.test*/);

        mVideoBusiness = new VideoBusiness(this);
        mVideoBusiness.initVideo(videoview, id_video_controller, mVideoUrl);
//		mVideoBusiness.initVideo(videoview,id_video_controller,uri);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        int screenWidth = ScreenUtil.getScreenWidth(this);
        setVideoContainerParam(screenWidth, screenWidth * 9 / 16);
        mVideoBusiness.startPlay();

    }

    private boolean isFullScreen;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
        //全屏看视频
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            int matchParent = ViewGroup.LayoutParams.MATCH_PARENT;
            setVideoContainerParam(matchParent, matchParent);
            if (StatusBarUtil.hasNavBar(this)) {
                StatusBarUtil.hideBottomUIMenu(this);
            }
            isFullScreen = true;
            StatusBarUtil.fullscreen(true, this);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { //从视频全屏界面恢复竖屏

            if (StatusBarUtil.hasNavBar(this)) {
                StatusBarUtil.showBottomUiMenu(this);
            }
            StatusBarUtil.fullscreen(false, this);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            int screenWidth = ScreenUtil.getScreenWidth(this);
            setVideoContainerParam(screenWidth, screenWidth * 9 / 16);
            isFullScreen = true;
        }
    }

    private void setVideoContainerParam(int w, int h) {
        ViewGroup.LayoutParams params = rl_video_container.getLayoutParams();
        params.width = w;
        params.height = h;
        rl_video_container.setLayoutParams(params);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isFullScreen) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                ImageView imageView = (ImageView) rl_video_container.findViewById(R.id.id_iv_video_expand);
                imageView.setImageResource(R.drawable.zuidahua_2x);
                return true;
            } else {
                finish();
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void initListener() {
        super.initListener();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.back) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVideoBusiness != null) {
//            mVideoBusiness.release();
        }
    }
}
