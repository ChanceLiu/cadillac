package com.mobile.cadillacsaler.base;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.entity.sale.SaleItemData;
import com.mobile.cadillacsaler.util.ScreenUtils;
import com.vise.utils.file.FileUtil;
import com.vise.utils.file.SdCardUtil;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by YoKeyword on 16/2/7.
 */
public class BaseBackFragment extends MySupportFragment {

    protected void initToolbarNav(Toolbar toolbar) {

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainActivity)_mActivity).requestAllPower();

    }

    public void showUser(boolean isShow) {
        ((MainActivity)_mActivity).showImgAccount(isShow);
    }

    public void showMsg(String title,String msg) {
        new AlertDialog.Builder(_mActivity)
                .setTitle(title)
                .setMessage(msg)
                .show();
    }

    @Override
    public void onSupportInvisible() {
        super.onSupportInvisible();
        hideSoftInput();
    }
}

