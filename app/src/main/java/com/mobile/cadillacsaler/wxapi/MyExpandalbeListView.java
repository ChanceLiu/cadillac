package com.mobile.cadillacsaler.wxapi;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ExpandableListView;

/**
 * Created by chanc on 2018/6/10.
 */

public class MyExpandalbeListView extends ExpandableListView{
    private int startX;
    private int startY;
    private float xLast;
    private float yLast;
    private float xDistance;
    private float yDistance;

    public MyExpandalbeListView(Context context) {
        super(context);
    }

    public MyExpandalbeListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xDistance = yDistance = 0f;
                xLast = ev.getX();
                yLast = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                final float curX = ev.getX();
                final float curY = ev.getY();

                xDistance += Math.abs(curX - xLast);
                yDistance += Math.abs(curY - yLast);
                xLast = curX;
                yLast = curY;

                if (xDistance > yDistance) {
                    return false;   //表示向下传递事件
                }
        }

        return super.onInterceptTouchEvent(ev);
    }
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        switch (ev.getAction()){
//            case MotionEvent.ACTION_DOWN:
//                startX = (int) ev.getRawX();
//                startY = (int) ev.getRawY();
//                break;
//            case MotionEvent.ACTION_MOVE:
//                int endX = (int) ev.getRawX();
//                int endY = (int) ev.getRawY();
//                if (Math.abs(endX - startX) > Math.abs(endY - startY)) {// 左右滑动
//                    return true;
//                }else{
//                    return super.onInterceptTouchEvent(ev);
//                }
//        }
//        return super.onInterceptTouchEvent(ev);
//    }
}
