package com.mobile.cadillacsaler.wxapi;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

/**
 * Created by chanc on 2018/6/10.
 */

public class MyGallery extends Gallery{
    private int startX;
    private int startY;

    public MyGallery(Context context) {
        super(context);
    }

    public MyGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        switch (ev.getAction()){
//            case MotionEvent.ACTION_DOWN:
//                startX = (int) ev.getRawX();
//                startY = (int) ev.getRawY();
//                break;
//            case MotionEvent.ACTION_MOVE:
//                int endX = (int) ev.getRawX();
//                int endY = (int) ev.getRawY();
//                if (Math.abs(endX - startX) > Math.abs(endY - startY)) {// 左右滑动
//                    return true;
//                }else{
//                    return super.onInterceptTouchEvent(ev);
//                }
//        }
//        return super.onInterceptTouchEvent(ev);
//    }
}
