package com.mobile.cadillacsaler.ui.fragment.practice;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.TrainVideoAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.practice.TrainVideoBean;
import com.mobile.cadillacsaler.entity.practice.TrainVideoData;
import com.mobile.cadillacsaler.listener.OnItemClickListener;
import com.mobile.cadillacsaler.util.PathUtil;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.utils.GSYVideoType;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.zlc.video.VideoPlayActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 培训视频 TrainVideo
 */
public class TrainVideo extends BaseBackFragment implements OnItemClickListener {

    public static final String TAG = TrainVideo.class.getSimpleName();
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recy)
    RecyclerView mRecy;
    @BindView(R.id.videoplayer)
    VideoView mVideoView;

    StandardGSYVideoPlayer gsyVideoPlayer;


    private String mTitle;
    private TrainVideoAdapter mAdapter;
    private ArrayList<TrainVideoBean> mItems;


    public static TrainVideo newInstance() {
        TrainVideo fragment = new TrainVideo();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trainvideo, container, false);

        ButterKnife.bind(this, view);
        initView(view);
        mItems = new TrainVideoData().data;
        mAdapter.setOnItemClickListener(this);
        mAdapter.setDatas(mItems);
        return view;
    }


    private void initView(View view) {

        tvToolbarTitle.setText("培训视频");


        mAdapter = new TrainVideoAdapter(this);
        LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);

    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }

    @Override
    public void onItemClick(int position, View view) {
//        Toast.makeText(_mActivity, mItems.get(position).toString(), Toast.LENGTH_SHORT).show();
//        play(PathUtil.sdcardPath + mItems.get(position).video);
        Intent intent = new Intent(_mActivity, VideoPlayActivity.class);
        intent.putExtra("path", PathUtil.sdcardPath + mItems.get(position).video);
        startActivity(intent);

    }


    @Override
    public void onResume() {
        super.onResume();
        //系统播放器
        GSYVideoManager.instance().setVideoType(_mActivity, GSYVideoType.IJKEXOPLAYER2);
    }

    private void play(String videoUrl) {
        mVideoView.setVisibility(View.VISIBLE);
        mVideoView.setVideoPath(videoUrl);
        mVideoView.start();


    }

    private void playVideo(String videoUrl) {
        gsyVideoPlayer.setVisibility(View.VISIBLE);

        gsyVideoPlayer.setUpLazy(videoUrl, false, null, null, "");
//增加title
        gsyVideoPlayer.getTitleTextView().setVisibility(View.GONE);
//设置返回键
        gsyVideoPlayer.getBackButton().setVisibility(View.VISIBLE);
//设置全屏按键功能
        gsyVideoPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gsyVideoPlayer.startWindowFullscreen(_mActivity, false, true);
            }
        });
//防止错位设置
//        gsyVideoPlayer.setPlayTag(TAG);
//        gsyVideoPlayer.setPlayPosition(position);
//是否根据视频尺寸，自动选择竖屏全屏或者横屏全屏
        gsyVideoPlayer.setAutoFullWithSize(true);
//音频焦点冲突时是否释放
        gsyVideoPlayer.setReleaseWhenLossAudio(true);
//全屏动画
        gsyVideoPlayer.setShowFullAnimation(true);
//小屏时不触摸滑动
        gsyVideoPlayer.setIsTouchWiget(false);
        gsyVideoPlayer.startWindowFullscreen(_mActivity,false,true);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        GSYVideoManager.releaseAllVideos();
    }
}
