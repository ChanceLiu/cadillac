package com.mobile.cadillacsaler.ui.fragment.sale;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.ExpandableListViewAdapter;
import com.mobile.cadillacsaler.adapter.SaleContentGroupAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.sale.SaleItemBean;
import com.mobile.cadillacsaler.entity.sale.SaleItemData;
import com.mobile.cadillacsaler.entity.sale.SaleSubItemBean;
import com.mobile.cadillacsaler.entity.sale.SaleSubItemData;
import com.mobile.cadillacsaler.widget.SaleItemView;
import com.tuacy.pinnedheader.PinnedHeaderItemDecoration;
import com.tuacy.pinnedheader.PinnedHeaderRecyclerView;
import com.tuacy.recyclerexpand.PatrolItem;
import com.tuacy.recyclerexpand.expand.ExpandGroupItemEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 了解产品
 */
public class SaleContent extends BaseBackFragment {

    public static final String TAG = SaleContent.class.getSimpleName();
    private static final String ARG_PARAMS = "items";
    private static final String ARG_PARAMS_TITLE = "title";
    private static final String ARG_PARAMS_TEST = "test";
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    LinearLayout mContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;

    @BindView(R.id.recyclerView)
    PinnedHeaderRecyclerView mRecyclerView;


    @BindView(R.id.epListView)
    ExpandableListView mEpListView;


    private String[] items;
    private LinearLayoutManager mLayoutManager;
    private SaleContentGroupAdapter mAdapter;
    private ExpandableListAdapter mAdapter2;
    private List<ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>> data;
    private String title;
    private boolean isTest;

    public static SaleContent newInstance(boolean isTest, String title, String... args) {

        SaleContent fragment = new SaleContent();
        Bundle bundle = new Bundle();
        bundle.putStringArray(ARG_PARAMS, args);
        bundle.putString(ARG_PARAMS_TITLE, title);
        bundle.putBoolean(ARG_PARAMS_TEST, isTest);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static SaleContent newInstance(String title, String... args) {

        SaleContent fragment = new SaleContent();
        Bundle bundle = new Bundle();
        bundle.putStringArray(ARG_PARAMS, args);
        bundle.putString(ARG_PARAMS_TITLE, title);
        bundle.putBoolean(ARG_PARAMS_TEST, false);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liaojiechanpin, container, false);

        ButterKnife.bind(this, view);

        Bundle arg = getArguments();
        items = arg.getStringArray(ARG_PARAMS);
        title = arg.getString(ARG_PARAMS_TITLE);
        isTest = arg.getBoolean(ARG_PARAMS_TEST);
        initView(view);
        initData();
//        initEvent();

        return view;
    }


    private void initView(View view) {

        tvToolbarTitle.setText(title);
        tvToolbarTitle.setTextColor(Color.GRAY);
    }


    private void initData() {
        data = obtainDataList();
        mAdapter2 = new ExpandableListViewAdapter(data, this);
        mEpListView.setAdapter(mAdapter2);

        mEpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (isTest) {
                    return true;
                }
                ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> entity = data.get(groupPosition);
                if ("试乘试驾".equals(entity.getParent().title)) {
                    start(DriveStep.newInstance());
                    return true;
                } else if ("三大卖点".equals(entity.getParent().title)) {
                    start(UniqueSkill.newInstance());
                    return true;
                } else if ("网络口碑".equals(entity.getParent().title)) {
                    start(OnlineComment.newInstance());
                } else if ("综合报价".equals(entity.getParent().title)) {
                    start(SalePomotion.newInstance());
                }
                return false;
            }
        });


    }


    private List<ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>> obtainDataList() {
        List<ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>> dataList = new ArrayList<>();
        for (String item : items) {
            dataList.add(getSpecItem(item));
        }
        return dataList;
    }

    /**
     * 获取8大选项
     *
     * @param item
     * @return
     */
    private ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> getSpecItem(String item) {
        ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> data = new ExpandGroupItemEntity<>();
        data.setParent(SaleItemData.getSaleItemBean(item));
        data.setChildList(SaleSubItemData.getSaleSubItem(item));


        return data;
    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }


}
