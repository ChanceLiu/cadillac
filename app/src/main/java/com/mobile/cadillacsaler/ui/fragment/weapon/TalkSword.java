package com.mobile.cadillacsaler.ui.fragment.weapon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.BrandManagerAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.brandManager.BrandData;
import com.mobile.cadillacsaler.entity.brandManager.BrandImageBean;
import com.mobile.cadillacsaler.listener.OnItemClickListener;
import com.mobile.cadillacsaler.util.PathUtil;
import com.zlc.video.VideoPlayActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 培训视频 TrainVideo
 */
public class TalkSword extends BaseBackFragment implements OnItemClickListener {

    public static final String TAG = TalkSword.class.getSimpleName();
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recy)
    RecyclerView mRecy;
    @BindView(R.id.videoplayer)
    VideoView mVideoView;
    @BindView(R.id.tv_item_0)
    TextView mTvItem0;
    @BindView(R.id.tv_item_1)
    TextView mTvItem1;


    private BrandManagerAdapter mAdapter;
    private ArrayList<BrandImageBean> imageBeans;
    private ArrayList<BrandImageBean> videoBeans;
    private int carTye = 0;


    public static TalkSword newInstance() {
        TalkSword fragment = new TalkSword();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_brand_manager, container, false);

        ButterKnife.bind(this, view);
        initView(view);
        imageBeans = BrandData.getImageData(carTye);
        videoBeans = BrandData.getVideoData(carTye);
        mAdapter.setOnItemClickListener(this);
        mAdapter.setDatas(videoBeans);
        return view;
    }


    private void initView(View view) {

        tvToolbarTitle.setText("品牌风范");


        mAdapter = new BrandManagerAdapter(this);
        LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);
        mTvItem0.setSelected(true);

    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }

    @Override
    public void onItemClick(int position, View view) {
//        Toast.makeText(_mActivity, mItems.get(position).toString(), Toast.LENGTH_SHORT).show();
//        play(PathUtil.sdcardPath + mItems.get(position).video);
        BrandImageBean item = mAdapter.getItem(position);
        if (item.isVideo) {
            Intent intent = new Intent(_mActivity, VideoPlayActivity.class);
            intent.putExtra("path", PathUtil.sdcardPath + item.video);
            startActivity(intent);
        }


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.tv_item_0, R.id.tv_item_1})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_item_0:
                mAdapter.setDatas(videoBeans);
                break;
            case R.id.tv_item_1:
                mAdapter.setDatas(imageBeans);
                break;
        }
        mAdapter.notifyDataSetChanged();
        selectItem(view);
    }


    public void selectItem(View view) {
        mTvItem0.setSelected(false);
        mTvItem1.setSelected(false);
        view.setSelected(true);

    }

}
