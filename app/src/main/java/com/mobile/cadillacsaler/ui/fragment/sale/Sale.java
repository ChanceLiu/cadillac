package com.mobile.cadillacsaler.ui.fragment.sale;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.Content;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.mobile.cadillacsaler.net.PromotionBean;
import com.mobile.cadillacsaler.net.SelfArrangeBean;
import com.tuacy.recyclerexpand.MainActivity;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class Sale extends BaseBackFragment {



    public static final String TAG = Sale.class.getSimpleName();
    private static final String ARG_TITLE = "arg_title";
    private static final String ARG_ITEMS = "arg_items";
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;


    private String mTitle;
    private ArrayList<Content> mItems;
    private ArrayList<SelfArrangeBean> items=new ArrayList<>();
    private SalePage1 salePage1;
    private SalePage2 salePage2;

    public static Sale newInstance(String title, ArrayList<Content> items) {
        Sale fragment = new Sale();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        bundle.putSerializable(ARG_ITEMS, items);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sale, container, false);

        ButterKnife.bind(this, view);
        initView(view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
            mItems = (ArrayList<Content>) bundle.getSerializable(ARG_ITEMS);
        }
        return view;
    }


    private void initView(View view) {

        tvToolbarTitle.setText("自我提升");
        tvToolbarTitle.setTextColor(Color.GRAY);
        initToolbarNav(toolbar);

        initPage();
    }

    ArrayList<View> views = new ArrayList<>();
    private void initPage() {

        salePage1 = new SalePage1(_mActivity);
        salePage1.sale = this;
        views.add(salePage1);

        salePage2 = new SalePage2(getContext());
        salePage2.sale = Sale.this;
        salePage2.setItems(items);

        viewPager.setAdapter(new MyViewPagerAdapter(views));

    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }


    @Override
    public void onSupportVisible() {
        super.onSupportVisible();
        getNetData();
        salePage2.setItems(items);

    }

    public void getNetData() {
        Map<String, String> params = new HashMap<>();
        params.put("action", Const.action_getAllMenus);
        params.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, Integer>() {
                    @Override
                    public Integer apply(Map<String, String> params) throws Exception {

                        NetBean bean = NetUtil.getNetData(params, PromotionBean.class, true);

                        ArrayList<Map<Object, Object>> data = (ArrayList<Map<Object, Object>>) bean.data;
                        items.clear();
                        try {
                            for (Map<Object, Object> map : data) {
                                SelfArrangeBean o = NetUtil.mapToObject(map, SelfArrangeBean.class);
                                items.add(o);
                            }
                        } catch (Exception e) {

                        }
                        int num = 0;
                        if (items.size() > 0) {
                            for (SelfArrangeBean item : items) {
                                if ("1".equals(item.valid)) {
                                    num++;
                                }
                            }

                        }
                        return num;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer count) {
                        if (count > 0) {
                            if (!views.contains(salePage2)) {
                                views.add(salePage2);
                            }
                            salePage2.setItems(items);
                        } else {
                            views.remove(salePage2);
                        }
                        viewPager.getAdapter().notifyDataSetChanged();

                    }
                });

    }


    public class MyViewPagerAdapter extends PagerAdapter {

        private ArrayList<View> datas;


        public MyViewPagerAdapter(ArrayList<View> datas) {
            this.datas = datas;
        }

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v  = datas.get(position);
            container.addView(v);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(datas.get(position));
        }
    }

}
