package com.mobile.cadillacsaler.ui.fragment.sale;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.xrecyclerview.XRecyclerView;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.CommonListAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.mobile.cadillacsaler.net.OnLineCommentBean;
import com.mobile.cadillacsaler.widget.MyTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * 网络口碑列表
 * <p>
 * 有分页
 * <p>
 * [Util dealNetAction:@"getNetAppraises" param:
 *
 * @{@"page":[NSString stringWithFormat:@"%d",pageValue],
 * @"type":typeString,
 * @"cartype":[NSNumber numberWithInt:KCarType]
 */
public class OnlineCommentList extends BaseBackFragment {


    @BindView(R.id.iv_toolbar_left)
    ImageView mIvToolbarLeft;
    @BindView(R.id.tv_toolbar_title)
    MyTextView mTvToolbarTitle;
    @BindView(R.id.recy)
    XRecyclerView mRecy;
    private String title;
    private CommonListAdapter mAdapter;
    private boolean isRefresh;
    private boolean isLoad;

    public static OnlineCommentList newInstance(String title) {
        OnlineCommentList fragment = new OnlineCommentList();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online_comment_list, container, false);

        ButterKnife.bind(this, view);
        initView(view);
        isRefresh = true;
        getNetData("0", title, CarType.currentType + "");
        return view;
    }

    public void getNetData(String page, String type, String cartype) {
        Map<String, String> params = new HashMap<>();
        params.put("action", "getNetAppraises");
        params.put("page", page);
        params.put("type", type);
        params.put("cartype", cartype);

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, ArrayList<OnLineCommentBean>>() {
                    @Override
                    public ArrayList<OnLineCommentBean> apply(Map<String, String> params) throws Exception {

                        NetBean bean = NetUtil.getNetData(params, OnLineCommentBean.class, true);

                        ArrayList<Map<Object, Object>> data = (ArrayList<Map<Object, Object>>) bean.data;
                        ArrayList<OnLineCommentBean> items = new ArrayList<>();
                        try {
                            for (Map<Object, Object> map : data) {
                                OnLineCommentBean o = NetUtil.mapToObject(map, OnLineCommentBean.class);
                                items.add(o);
                            }
                        } catch (Exception e) {

                        }

                        return items;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ArrayList<OnLineCommentBean>>() {
                    @Override
                    public void accept(ArrayList<OnLineCommentBean> userBean) {
                        if (isRefresh) {
                            mRecy.refreshComplete();
                        }

                        if (userBean.size() != 0) {
                            data.clear();
                            data.addAll(userBean);
                            mAdapter.notifyDataSetChanged();
                        }

                        if (isLoad) {
                            mRecy.noMoreLoading();
                        }
                    }
                });

    }

    ArrayList<OnLineCommentBean> data = new ArrayList<>();

    private void initView(View view) {
        title = getArguments().getString("title");
        mTvToolbarTitle.setText(title);
//        mTvToolbarTitle.setTextColor(Color.GRAY);
        mTvToolbarTitle.setTextColor(getResources().getColor(R.color.textLightGray));
        initRecycleView();

    }


    private void initRecycleView() {
        mAdapter = new CommonListAdapter(this, data);
        final LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);


        mRecy.setLoadingMoreEnabled(true);
        mRecy.setPullRefreshEnabled(true);

        mRecy.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
                isLoad = false;
                currentPage = 0;
                getNetData(currentPage + "", title, CarType.currentType + "");
            }

            @Override
            public void onLoadMore() {
                isRefresh = false;
                isLoad = true;
                getNetData(++currentPage + "", title, CarType.currentType + "");

            }
        });


    }

    private int currentPage = 0;


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();

    }
}
