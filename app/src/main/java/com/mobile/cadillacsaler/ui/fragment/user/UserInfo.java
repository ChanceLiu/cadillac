package com.mobile.cadillacsaler.ui.fragment.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.ui.fragment.SaleLogin;
import com.mobile.cadillacsaler.ui.fragment.start.Start;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.vise.xsnow.util.SharePreferenceUtil;
import com.vise.xsnow.widget.GlideCircleTransform;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 用户中心
 */
public class UserInfo extends BaseBackFragment {


    @BindView(R.id.iv_toolbar_left)
    ImageView mIvToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView mIvToolbarLogo;
    @BindView(R.id.tv_left)
    MyTextView mTvLeft;
    @BindView(R.id.tv_toolbar_title)
    MyTextView mTvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView mIvToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.iv)
    ImageView mIv;
    @BindView(R.id.work)
    MyTextView mWork;
    @BindView(R.id.username)
    MyTextView mUsername;
    @BindView(R.id.tv_item_0)
    MyTextView mTvItem0;
    @BindView(R.id.tv_item_1)
    MyTextView mTvItem1;
    @BindView(R.id.tv_item_2)
    MyTextView mTvItem2;
    @BindView(R.id.tv_item_3)
    MyTextView mTvItem3;
    @BindView(R.id.tv_item_4)
    MyTextView mTvItem4;
    @BindView(R.id.tv_exit)
    MyTextView mTvExit;

    public static UserInfo newInstance() {
        UserInfo fragment = new UserInfo();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) _mActivity).showImgAccount(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) _mActivity).showImgAccount(true);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_userinfo, container, false);

        ButterKnife.bind(this, view);

        initView(view);
        return view;
    }


    private void initView(View view) {

        mTvToolbarTitle.setText("个人中心");
        Glide.with(_mActivity)
                .load(R.mipmap.head_icon)
                .transform(new GlideCircleTransform(_mActivity))
                .into(mIv);

    }

    @OnClick({R.id.iv_toolbar_left, R.id.tv_item_0, R.id.tv_item_1, R.id.tv_item_2, R.id.tv_item_3, R.id.tv_item_4, R.id.tv_exit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_toolbar_left:
                pop();
                break;
            case R.id.tv_item_0:
                start(BeiWangLu.newInstance());
                break;
            case R.id.tv_item_1:
                start(KeHuXinXi.newInstance());
                break;
            case R.id.tv_item_2:
                start(MySetting.newInstance());
                break;
            case R.id.tv_item_3:
                Toast.makeText(_mActivity, "车型选择", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_item_4:
                start(FeedBack.newInstance());
                break;
            case R.id.tv_exit:
//                Toast.makeText(_mActivity, "开发中", Toast.LENGTH_SHORT).show();
                SharePreferenceUtil.setParam(_mActivity, SpKey.KEY_UID, "");
                start(SaleLogin.newInstance());
                popTo(SaleLogin.class, false);

//                startWithPop(SupportFragment fragment, Class targetFragment, boolean includeTargetFragment)
//                startWithPopTo(SupportFragment fragment, Class targetFragment, boolean includeTargetFragment)
//                ((SupportActivity)_mActivity).startWithPop(SaleLogin.newInstance(),);
                break;
        }
    }


    @OnClick({R.id.car1, R.id.car2, R.id.car3, R.id.car4})
    public void onClick2(View view) {
        switch (view.getId()) {
            case R.id.car1:
                CarType.currentType = CarType.ats_l;
                break;
            case R.id.car2:
                CarType.currentType = CarType.xts;
                break;
            case R.id.car3:
                CarType.currentType = CarType.ct6;
                break;
            case R.id.car4:
                CarType.currentType = CarType.xt5;
                break;
        }
        popTo(Start.class, false);
        SharePreferenceUtil.setParam(_mActivity, SpKey.KEY_CAR_TYPE, CarType.currentType);
    }
}
