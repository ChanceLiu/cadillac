package com.mobile.cadillacsaler.ui.fragment.start;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.SecondPageAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.Content;
import com.mobile.cadillacsaler.listener.OnItemClickListener;
import com.mobile.cadillacsaler.ui.fragment.DetailFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class Introduce extends BaseBackFragment implements OnItemClickListener {

    public static final String TAG = Introduce.class.getSimpleName();
    private static final String ARG_TITLE = "arg_title";
    private static final String ARG_ITEMS = "arg_items";
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recy)
    RecyclerView mRecy;


    private String mTitle;
    private SecondPageAdapter mAdapter;
    private ArrayList<Content> mItems;

    public static Introduce newInstance(String title, ArrayList<Content> items) {
        Introduce fragment = new Introduce();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        bundle.putSerializable(ARG_ITEMS, items);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_page, container, false);

        ButterKnife.bind(this, view);
        initView(view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
            mItems = (ArrayList<Content>) bundle.getSerializable(ARG_ITEMS);
        }
        mAdapter.setOnItemClickListener(this);
        mAdapter.setDatas(mItems);
        return view;
    }


    private void initView(View view) {

        tvToolbarTitle.setText(mTitle);

        initToolbarNav(toolbar);

        mAdapter = new SecondPageAdapter(_mActivity);
        LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                start(DetailFragment.newInstance(mAdapter.getItem(position).getTitle()));
            }
        });
    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }

    @Override
    public void onItemClick(int position, View view) {
        Toast.makeText(_mActivity, mItems.get(position).toString(), Toast.LENGTH_SHORT).show();
    }
}
