package com.mobile.cadillacsaler.ui.fragment.practice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.practice.SelfLearnBean;
import com.mobile.cadillacsaler.util.PathUtil;
import com.mobile.cadillacsaler.widget.LongPicView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.onekeyshare.OnekeyShare;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class SelfLearnDetail extends BaseBackFragment {

    private static final String ARG_ITEMS = "arg_items";
    @BindView(R.id.image)
    ImageView mImage;
    @BindView(R.id.drump)
    ImageButton mDrump;
    @BindView(R.id.btn_close)
    ImageButton mBtnClose;
    @BindView(R.id.btn_share)
    ImageButton mBtnShare;
    @BindView(R.id.longImageView)
    LongPicView mLongView;
    private SelfLearnBean selfLearnBean;

    public static SelfLearnDetail newInstance(SelfLearnBean selfLearnBean) {
        SelfLearnDetail fragment = new SelfLearnDetail();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_ITEMS, selfLearnBean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_self_learn_detail, container, false);

        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            selfLearnBean = (SelfLearnBean) bundle.getSerializable(ARG_ITEMS);
        }
        initView(view);
        return view;
    }


    private void initView(View view) {
        mDrump.setVisibility(selfLearnBean.toOtherPage?View.VISIBLE:View.GONE);

            mLongView.setImageSource(PathUtil.sdcardPath+selfLearnBean.image);

    }

    private void showImg(String fileName) {
        Observable.fromArray(fileName)
                .map(new Function<String, byte[]>() {
                    @Override
                    public byte[] apply(String fileName) throws Exception {
                        byte[] bytes = PathUtil.input2byte(_mActivity.getAssets().open(fileName));
                        return bytes;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<byte[]>() {
                    @Override
                    public void accept(byte[] bytes) throws Exception {
                        //                        Glide.with(SelfLearnDetail.this)
//                                .load(bytes)
//                                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                                .into(mImage);
                    }
                });
    }

    @Override
    public void onSupportVisible() {
        super.onSupportVisible();
        ((MainActivity) _mActivity).showImgAccount(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) _mActivity).showImgAccount(true);

    }

    @OnClick({R.id.image, R.id.drump, R.id.btn_close, R.id.btn_share})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image:
                break;
            case R.id.drump:
                break;
            case R.id.btn_close:
                pop();
                break;
            case R.id.btn_share:
                showShare();
                break;
        }
    }
    private void showShare() {
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
//        oks.disableSSOWhenAuthorize();

        // title标题，微信、QQ和QQ空间等平台使用
        oks.setTitle("分享");
        // titleUrl QQ和QQ空间跳转链接
        oks.setTitleUrl("http://sharesdk.cn");
        // text是分享文本，所有平台都需要这个字段
        oks.setText("我是分享文本");
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        oks.setImagePath(PathUtil.sdcardPath + selfLearnBean.image);//确保SDcard下面存在此张图片
        // url在微信、微博，Facebook等平台中使用
        oks.setUrl("http://sharesdk.cn");
        // comment是我对这条分享的评论，仅在人人网使用
        oks.setComment("我是测试评论文本");
        // 启动分享GUI
        oks.show(_mActivity);
    }
}
