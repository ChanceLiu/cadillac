package com.mobile.cadillacsaler.ui.fragment.sale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.xrecyclerview.XRecyclerView;
import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.adapter.SelfArrangeAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.net.KeHuXinXiBean;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.mobile.cadillacsaler.net.PromotionBean;
import com.mobile.cadillacsaler.net.SelfArrangeBean;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * 综合报价
 */
public class SelfArrange extends BaseBackFragment {


    @BindView(R.id.iv_toolbar_left)
    ImageView mIvToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView mIvToolbarLogo;
    @BindView(R.id.tv_left)
    MyTextView mTvLeft;
    @BindView(R.id.tv_toolbar_title)
    MyTextView mTvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView mIvToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.recy)
    XRecyclerView mRecy;
    @BindView(R.id.add)
    ImageView mAdd;
    private List<SelfArrangeBean> data = new ArrayList<>();
    private SelfArrangeAdapter mAdapter;

    public static SelfArrange newInstance() {
        SelfArrange fragment = new SelfArrange();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_self_arrange, container, false);

        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        initView(view);
        return view;
    }


    private void initView(View view) {
        mTvToolbarTitle.setTextColor(Color.GRAY);
        mTvToolbarTitle.setText("个性定制");
        mIvToolbarRight.setVisibility(View.VISIBLE);
        mIvToolbarRight.setImageResource(R.mipmap.self_arrange_add_btn);
        ((MainActivity) _mActivity).showImgAccount(false);


        initRecycleView();


    }

    @Override
    public void onSupportVisible() {
        super.onSupportVisible();
        getNetData();
    }

    public void getNetData() {
        Map<String, String> params = new HashMap<>();
        params.put("action", Const.action_getAllMenus);
        params.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, ArrayList<SelfArrangeBean>>() {
                    @Override
                    public ArrayList<SelfArrangeBean> apply(Map<String, String> params) throws Exception {

                        NetBean bean = NetUtil.getNetData(params, PromotionBean.class, true);

                        ArrayList<Map<Object, Object>> data = (ArrayList<Map<Object, Object>>) bean.data;
                        ArrayList<SelfArrangeBean> items = new ArrayList<>();
                        try {
                            for (Map<Object, Object> map : data) {
                                SelfArrangeBean o = NetUtil.mapToObject(map, SelfArrangeBean.class);
                                items.add(o);
                            }
                        } catch (Exception e) {

                        }

                        return items;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ArrayList<SelfArrangeBean>>() {
                    @Override
                    public void accept(ArrayList<SelfArrangeBean> userBean) {
                        data.clear();
                        data.addAll(userBean);
                        mAdapter.notifyDataSetChanged();
                    }
                });

    }


    private void initRecycleView() {
        mAdapter = new SelfArrangeAdapter(this, data);
        final LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);


        mRecy.setLoadingMoreEnabled(false);
        mRecy.setPullRefreshEnabled(false);

    }

    @OnClick(R.id.iv_toolbar_left)
    public void back() {
        pop();
    }

    @OnClick(R.id.iv_toolbar_right)
    public void add() {
        start(SelfArrangeItemEdit.newInstance(new SelfArrangeBean()));
    }

    @OnClick(R.id.add)
    public void addToMainPage() {

        if (data.size() == 0) {
            showMsg("不存在条目","请点击右上角 + 按钮进行添加");
            return;
        }


        ArrayList<SelfArrangeBean> selected = new ArrayList<>();
        for (SelfArrangeBean datum : data) {
            if ("1".equals(datum.valid)) {
                selected.add(datum);
            }
        }

        if (selected.size()>4) {
            Toast.makeText(_mActivity, "不能超过4个选项", Toast.LENGTH_SHORT).show();
            return;
        }
        if (selected.size() == 0) {
            showMsg("未选中","请点击条目左侧圆形按钮,选择条目");
            return;
        }

        upload(selected);

    }

    public void upload(ArrayList<SelfArrangeBean> selected ) {

        Observable.fromArray(selected)
                .subscribeOn(Schedulers.io())
                .map(new Function<ArrayList<SelfArrangeBean>, Boolean>() {
                    @Override
                    public Boolean apply(ArrayList<SelfArrangeBean> data1) throws Exception {
                        Map<String, String> params = new HashMap<>();
                        params.put("action",Const.action_updateMenus);
                        params.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");
                        for (SelfArrangeBean item : data) {
                            params.put("name", item.name);
                            params.put("menuslist", item.menustxt);
                            params.put("valid", item.valid);
                            params.put("id", item.id);

                            NetBean bean = NetUtil.getNetData(params, String.class, false);
                            if (bean.code != 200) {
                                return false;
                            }
                        }




                        Map<String, String> params2 = new HashMap<>();
                        params2.put("action",Const.action_setIndexMenus);
                        params2.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");
                        String v = "";
                        for (int i = 0; i < data.size(); i++) {
                            SelfArrangeBean selfArrangeBean = data.get(i);
                            if ("1".equals(selfArrangeBean.valid)) {
                                v += selfArrangeBean.id+",";
                            }

                        }
                        params2.put("valids", v);
                        params2.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");
                        NetBean bean2 = NetUtil.getNetData(params2, String.class, false);
                        if (bean2.code != 200) {
                            return false;
                        }

                        return true;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean userBean) {
                        if (userBean) {
                            Toast.makeText(_mActivity, "添加成功", Toast.LENGTH_SHORT).show();
                            pop();
                        } else {
                            Toast.makeText(_mActivity, "添加失败,请重试", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    public void delCust(final SelfArrangeBean bean) {
        new AlertDialog.Builder(_mActivity)
                .setTitle("删除")
                .setMessage("\n删除这个条目么?")
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delCust(bean.id);
                    }
                }).show();

    }

    public void delCust(String id) {

        Map<String, String> params = new HashMap<>();
        params.put("action", Const.action_delMenus);
        params.put("id", id+"");
        params.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, NetBean>() {
                    @Override
                    public NetBean apply(Map<String, String> params) throws Exception {
                        NetBean bean = NetUtil.getNetData(params, KeHuXinXiBean.class, false);
                        return bean;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<NetBean>() {
                    @Override
                    public void accept(NetBean bean) {
                        if (bean.code == 200) {
                            Toast.makeText(_mActivity, "删除成功", Toast.LENGTH_SHORT).show();
                            getNetData();
                        }
                    }
                });

    }


}
