package com.mobile.cadillacsaler.ui.fragment.sale;

import android.view.View;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.widget.SaleItemView;

/**
 *  产品销售八大选项点击事件
 */

public class SaleItemClickListener implements View.OnClickListener {
    public MySupportFragment fragment;

    public SaleItemClickListener(MySupportFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.mipmap.sale_tab_btn_0_0_3x:
//0 车型亮点
                ((SaleItemView)v).togle();


                break;
            case R.mipmap.sale_tab_btn_1_0_3x:
//1 车款配置


                break;
            case R.mipmap.sale_tab_btn_2_0_3x:
                //2 三大卖点

                //三大卖点
                fragment.start(UniqueSkill.newInstance());

                break;
            case R.mipmap.sale_tab_btn_3_0_3x:
                //3 试乘试驾
                fragment.start(DriveStep.newInstance());


                break;
            case R.mipmap.sale_tab_btn_4_0_3x:
                //4 综合报价



                break;
            case R.mipmap.sale_tab_btn_5_0_3x:
                //5 竞品对比




                break;
            case R.mipmap.sale_tab_btn_6_0_3x:
                //6 网络口碑


                break;
            case R.mipmap.sale_tab_btn_7_0_3x:
                //7 自我对比



                break;
        }
    }


}
