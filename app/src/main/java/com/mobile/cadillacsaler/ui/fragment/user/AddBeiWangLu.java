package com.mobile.cadillacsaler.ui.fragment.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.widget.MyEditText;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class AddBeiWangLu extends BaseBackFragment {

    public static final String TAG = AddBeiWangLu.class.getSimpleName();
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_content)
    MyEditText mEtContent;
    @BindView(R.id.submit)
    ImageView mSubmit;
    @BindView(R.id.title)
    MyEditText mTitle;


    public static AddBeiWangLu newInstance() {
        AddBeiWangLu fragment = new AddBeiWangLu();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_beiwanglu, container, false);

        ButterKnife.bind(this, view);
        initView(view);


        return view;
    }



    private void initView(View view) {

        tvToolbarTitle.setText("添加备忘录");


    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }


    /*
    * [Util dealNetAction:@"addLetter" param: @{@"content":_textView.text,@"contact":_textField.text}]
    * */
    private void addBeiwanglu(String content, String title, String uid) {
        OkHttpClient client = new OkHttpClient.Builder().build();

        RequestBody body = new FormBody.Builder()
                .add("content", content)
                .add("title", title)
                .add("uid", uid)
                .add("action", Const.action_addMessage)
                .build();
        final Request request = new Request
                .Builder()
                .url(Const.baseUrl)
                .post(body)
                .build();


        client
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        Toast.makeText(_mActivity, "添加失败!请重试", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String result = response.body().string();
                        _mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(_mActivity, "添加成功!!", Toast.LENGTH_SHORT).show();
                                pop();
                            }
                        });
                    }
                });

    }


    @OnClick(R.id.submit)
    public void submit() {
        String title = mTitle.getText().toString();
        String content = mEtContent.getText().toString();

        if (TextUtils.isEmpty(content)) {
            Toast.makeText(_mActivity, "反馈内容不能为空!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(title)) {
            Toast.makeText(_mActivity, "联系方式不能为空!!", Toast.LENGTH_SHORT).show();
            return;
        }

        addBeiwanglu(content, title, SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "").toString());
    }


}
