package com.mobile.cadillacsaler.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.mobile.cadillacsaler.net.UserBean;
import com.mobile.cadillacsaler.ui.fragment.start.Start;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by YoKeyword on 16/2/14.
 */
public class SaleLogin extends BaseBackFragment {
    private EditText mEtAccount, mEtPassword;
    private ImageButton mBtnLogin;


    public static SaleLogin newInstance() {

        Bundle args = new Bundle();

        SaleLogin fragment = new SaleLogin();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        initView(view);

        return view;
    }

    private void initView(View view) {
        mEtAccount = view.findViewById(R.id.et_account);
        mEtPassword = view.findViewById(R.id.et_password);
        mBtnLogin = view.findViewById(R.id.btn_login);


        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                NetUtil.feedback("test","test","25723");
//                login("CD1000","123456");

                String strAccount = mEtAccount.getText().toString();
                String strPassword = mEtPassword.getText().toString();
                if (TextUtils.isEmpty(strAccount.trim())) {
                    Toast.makeText(_mActivity, R.string.error_username, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(strPassword.trim())) {
                    Toast.makeText(_mActivity, R.string.error_pwd, Toast.LENGTH_SHORT).show();
                    return;
                }

//                login("CD1000","123456");
                login(strAccount, strPassword);
            }
        });

//        mBtnRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                start(RegisterFragment.newInstance());
//            }
//        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnLoginSuccessListener {
        void onLoginSuccess(String account);
    }

    @Override
    public void onSupportInvisible() {
        super.onSupportInvisible();
        hideSoftInput();
    }

    @Override
    public boolean onBackPressedSupport() {
        ((MainActivity)_mActivity).exit();
        return true;
    }

    private void login(String tel, String upwd) {

        Map<String, String> params = new HashMap<>();
        params.put("action", "login");
        params.put("tel", tel);
        params.put("upwd", upwd);

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, NetBean>() {
                    @Override
                    public NetBean apply(Map<String, String> params) throws Exception {

                        NetBean bean = NetUtil.getNetData(params, UserBean.class, false);

                        return bean;
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<NetBean>() {
                    @Override
                    public void accept(NetBean userBean) throws Exception {
                        if (userBean == null) {
                            Toast.makeText(_mActivity, "请求失败,请检测网络重试", Toast.LENGTH_SHORT).show();
                        } else {
                            UserBean user = NetUtil.mapToObject((Map<Object, Object>) userBean.data, UserBean.class);
                            if (user == null) {
                                Toast.makeText(_mActivity, userBean.msg, Toast.LENGTH_SHORT).show();
                            } else {
                                SharePreferenceUtil.setParam(_mActivity, SpKey.KEY_UID, user.uid);
                                startWithPop(Start.newInstance());
                            }
                        }


                    }
                });


//
//        OkHttpClient client = new OkHttpClient.Builder().build();
//
//
//        RequestBody body = new FormBody.Builder()
//                .add("tel", tel)
//                .add("upwd", upwd)
//                .add("action", "login")
//                .build();
//        final Request request = new Request
//                .Builder()
//                .url("http://cadillac.sh-jinger.com/")
//                .post(body).build();
//
//
//        client
//                .newCall(request)
//                .enqueue(new Callback() {
//                    @Override
//                    public void onFailure(Call call, IOException e) {
//                        e.printStackTrace();
//                        System.out.println();
//                    }
//
//                    @Override
//                    public void onResponse(Call call, Response response) throws IOException {
//                        String result = response.body().string();
//                        System.out.println(result);
//                    }
//                });

    }

}
