package com.mobile.cadillacsaler.ui.fragment.sale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.sale.SaleItemData;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.mobile.cadillacsaler.net.PromotionBean;
import com.mobile.cadillacsaler.net.SelfArrangeBean;
import com.mobile.cadillacsaler.util.ScreenUtils;
import com.mobile.cadillacsaler.widget.MyEditText;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.mobile.cadillacsaler.widget.dragexpandgrid.model.DragIconInfo;
import com.mobile.cadillacsaler.widget.dragexpandgrid.view.CustomGroup;
import com.vise.xsnow.event.BusManager;
import com.vise.xsnow.event.Subscribe;
import com.vise.xsnow.event.inner.ThreadMode;
import com.vise.xsnow.ui.adapter.BaseAdapter;
import com.vise.xsnow.ui.adapter.BaseViewHolder;
import com.vise.xsnow.util.SharePreferenceUtil;
import com.zlc.video.utils.DensityUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * 自定义画面列表  selfArrange文件夹
 */
public class SelfArrangeItemEdit extends BaseBackFragment {

    public static final String TAG = SelfArrangeItemEdit.class.getSimpleName();
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_right)
    MyTextView mTvToolbarRight;
    @BindView(R.id.list_view1)
    CustomGroup mListView1;
    @BindView(R.id.list_view2)
    GridView mListView2;
    private BaseAdapter adapter;
    private Dialog dialog;
    private SelfArrangeBean item;


    public static SelfArrangeItemEdit newInstance(SelfArrangeBean arrangeBean) {
        SelfArrangeItemEdit fragment = new SelfArrangeItemEdit();
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", arrangeBean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusManager.getBus().register(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusManager.getBus().unregister(this);

    }

    @Override
    public void onSupportVisible() {
        super.onSupportVisible();
        showUser(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selfarrangeitemedit, container, false);

        ButterKnife.bind(this, view);

        initView(view);
        initData();
        return view;
    }

    private void initData() {
        initAll();
        item = (SelfArrangeBean) getArguments().getSerializable("item");
        if (!TextUtils.isEmpty(item.menustxt)) {
            String[] menus = item.menustxt.split(",");

            for (int i = 0; i < menus.length; i++) {
                    DragIconInfo selected = all.get(Integer.parseInt(menus[i]));
                    selected.isChoose = true;
                    items1.add(selected);
            }
            all.removeAll(items1);

        }

        items2.addAll(all);

        mListView1.setIconInfoList(items1);

//        mListView2.setIconInfoList(items2);
        adapter = new BaseAdapter<DragIconInfo>(_mActivity, items2, R.layout.gridview_above_itemview) {


            @Override
            public <H extends BaseViewHolder> void convert(H viewHolder, int position, final DragIconInfo dragIconInfo) {
                ImageView icon = viewHolder.getView(R.id.icon_iv);
                icon.setImageResource(dragIconInfo.getResIconId2());
            }
        };
        mListView2.setAdapter(adapter);
        mListView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onDragItemClick(items2.get(position));
            }
        });
        mListView2.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                show(items2.get(position).getId());
                return true;
            }
        });


    }


    @Subscribe(threadMode = ThreadMode.MAIN_THREAD)
    public void onDragItemClick(DragIconInfo dragIconInfo) {
        items1.remove(dragIconInfo);
        items2.remove(dragIconInfo);


        dragIconInfo.isChoose = !dragIconInfo.isChoose;
        if (dragIconInfo.isChoose) {
            items1.add(dragIconInfo);
        } else {
            items2.add(dragIconInfo);
        }
        mListView1.setIconInfoList(items1);
        //mListView2.setIconInfoList(items2);
        adapter.notifyDataSetChanged();

    }

    private void initAll() {
        all.add(new DragIconInfo(0, R.mipmap.self_arrange_icon_0_1, R.mipmap.self_arrange_icon_0_0, false));
        all.add(new DragIconInfo(1, R.mipmap.self_arrange_icon_1_1, R.mipmap.self_arrange_icon_1_0, false));
        all.add(new DragIconInfo(2, R.mipmap.self_arrange_icon_2_1, R.mipmap.self_arrange_icon_2_0, false));
        all.add(new DragIconInfo(3, R.mipmap.self_arrange_icon_3_1, R.mipmap.self_arrange_icon_3_0, false));
        all.add(new DragIconInfo(4, R.mipmap.self_arrange_icon_4_1, R.mipmap.self_arrange_icon_4_0, false));
        all.add(new DragIconInfo(5, R.mipmap.self_arrange_icon_5_1, R.mipmap.self_arrange_icon_5_0, false));
        all.add(new DragIconInfo(6, R.mipmap.self_arrange_icon_6_1, R.mipmap.self_arrange_icon_6_0, false));
        all.add(new DragIconInfo(7, R.mipmap.self_arrange_icon_7_1, R.mipmap.self_arrange_icon_7_0, false));
    }

    ArrayList<DragIconInfo> all = new ArrayList<>();
    ArrayList<DragIconInfo> items1 = new ArrayList<>();
    ArrayList<DragIconInfo> items2 = new ArrayList<>();


    private void initView(View view) {

        tvToolbarTitle.setText("自定义排放顺序");
        tvToolbarTitle.setTextColor(getResources().getColor(R.color.lightGray));
        ivToolbarRight.setImageResource(R.mipmap.title_right);
        ivToolbarRight.setVisibility(View.VISIBLE);


    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }

    public void upload(String name) {


        Map<String, String> params = new HashMap<>();
        boolean isNew = TextUtils.isEmpty(item.id);
        params.put("action", isNew ? Const.action_addMenus : Const.action_updateMenus);
        params.put("name", name);
        params.put("menuslist", getData().menustxt);
        String value = isNew?"0":item.valid;
        params.put("valid", value);
        if (!isNew) {

            params.put("id", item.id);
        }
        params.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, NetBean>() {
                    @Override
                    public NetBean apply(Map<String, String> params) throws Exception {

                        NetBean bean = NetUtil.getNetData(params, String.class, false);

                        return bean;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<NetBean>() {
                    @Override
                    public void accept(NetBean userBean) {
                        if (200 == userBean.code) {
                            Toast.makeText(_mActivity, "添加成功", Toast.LENGTH_SHORT).show();
                            pop();
                        } else {
                            Toast.makeText(_mActivity, "添加失败,请重试", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }

    public void save() {
        final EditText et = new MyEditText(_mActivity);
        et.setText(item.name);
        et.setHint("请输入名称");

        et.setGravity(Gravity.CENTER);
        new AlertDialog.Builder(_mActivity)
                .setTitle("添加名称")
                .setMessage("可填写中文英文名字")
                .setView(et)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String s = et.getText().toString();
                        if (TextUtils.isEmpty(s) || s.length() > 6) {
                            Toast.makeText(_mActivity, "名字不能为空,且不能超过6个字符", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        upload(s);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void look() {
        String[] data = getIds();
        start(SaleContent.newInstance(true,
                "预览",
                data
        ));

    }

    String[] getIds() {
        ArrayList<Integer> integers = new ArrayList<>();
        for (DragIconInfo dragIconInfo : items1) {
            if (dragIconInfo.isChoose) {
                integers.add(dragIconInfo.getId());
            }
        }

        String[] ints = new String[integers.size()];

        for (int i = 0; i < integers.size(); i++) {
            ints[i] = SaleItemData.getItemName(integers.get(i));
        }
        return ints;
    }

    SelfArrangeBean getData() {
        StringBuilder sb = new StringBuilder();
        for (DragIconInfo dragIconInfo : items1) {
            if (dragIconInfo.isChoose) {
                sb.append(dragIconInfo.getId() + ",");
            }
        }

        item.menustxt = sb.toString();
        if (TextUtils.isEmpty(item.menustxt)) {
            item.menustxt = item.menustxt.substring(0, item.menustxt.length() - 1);
        }


        return item;
    }

    @OnClick(R.id.iv_toolbar_right)
    public void menu() {
        dialog = new Dialog(_mActivity, R.style.CustomDialog);
        final View dialogView = LayoutInflater.from(_mActivity).inflate(R.layout.dialog, null);
        final TextView save = (TextView) dialogView.findViewById(R.id.item1);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                dialog.dismiss();
            }
        });
        TextView look = (TextView) dialogView.findViewById(R.id.item2);
        look.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                look();
                dialog.dismiss();
            }
        });
        TextView cancel = (TextView) dialogView.findViewById(R.id.item3);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        //获得dialog的window窗口
        Window window = dialog.getWindow();
        //设置dialog在屏幕底部
        window.setGravity(Gravity.BOTTOM);
        //设置dialog弹出时的动画效果，从屏幕底部向上弹出
        //        window.setWindowAnimations(R.style.dialogStyle);
        window.getDecorView().setPadding(DensityUtil.dp2px(_mActivity, 32), 0, DensityUtil.dp2px(_mActivity, 32), 0);

        //获得window窗口的属性
        WindowManager.LayoutParams lp = window.getAttributes();
        //设置窗口宽度为充满全屏
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //设置窗口高度为包裹内容
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //将设置好的属性set回去
        window.setAttributes(lp);
        //将自定义布局加载到dialog上
        dialog.setContentView(dialogView);

        dialog.show();


    }


    public void show(int index) {
        new AlertDialog.Builder(_mActivity)
                .setTitle(SaleItemData.getItemName(index) + "   简述")
                .setMessage(SaleItemData.getItemDesc(index))
                .show();
    }





}
