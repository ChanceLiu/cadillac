package com.mobile.cadillacsaler.ui.fragment.sale;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.util.PathUtil;
import com.mobile.cadillacsaler.widget.MyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.onekeyshare.OnekeyShare;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * 网络口碑
 */
public class OnlineComment extends BaseBackFragment {


    @BindView(R.id.iv_toolbar_left)
    ImageView mIvToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView mIvToolbarLogo;
    @BindView(R.id.tv_left)
    MyTextView mTvLeft;
    @BindView(R.id.tv_toolbar_title)
    MyTextView mTvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView mIvToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.btn0)
    ImageButton mBtn0;
    @BindView(R.id.btn1)
    ImageButton mBtn1;
    @BindView(R.id.btn2)
    ImageButton mBtn2;
    @BindView(R.id.btn3)
    ImageButton mBtn3;
    @BindView(R.id.btn4)
    ImageButton mBtn4;
    @BindView(R.id.btn5)
    ImageButton mBtn5;
    @BindView(R.id.btn06)
    ImageButton mBtn06;
    @BindView(R.id.btn8)
    ImageButton mBtn8;
    @BindView(R.id.btn7)
    ImageButton mBtn7;
    @BindView(R.id.image)
    ImageView image;

    public static OnlineComment newInstance() {
        OnlineComment fragment = new OnlineComment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online_comment, container, false);

        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        initView(view);
        return view;
    }


    private void initView(View view) {
        mTvToolbarTitle.setTextColor(Color.GRAY);
        mTvToolbarTitle.setText("网络口碑");

        switch (CarType.currentType) {
            case 0:
                image.setImageResource(R.mipmap.online_comment_type_0);
                break;
            case 1:
                image.setImageResource(R.mipmap.online_comment_type_1);
                break;
            case 2:
                image.setImageResource(R.mipmap.online_comment_type_2);
                break;
            case 3:
                image.setImageResource(R.mipmap.online_comment_type_3);
                break;
        }
    }


    @OnClick(R.id.iv_toolbar_left)
    public void back() {
        pop();
    }


    @OnClick({R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn06, R.id.btn8, R.id.btn7})
    public void onClick(View view) {
        String params = "";
        switch (view.getId()) {
            case R.id.btn0:
                params = "综合口碑印象";
                break;
            case R.id.btn1:
                params = "性价比";
                break;
            case R.id.btn2:
                params = "外观";
                break;
            case R.id.btn3:
                params = "舒适性";
                break;
            case R.id.btn4:
                params = "内饰";
                break;
            case R.id.btn5:
                params = "操控";
                break;
            case R.id.btn06:
                params = "耗油";

                break;
            case R.id.btn7:
                params = "空间";
                break;
            case R.id.btn8:
                params = "动力";
                break;
        }

        start(OnlineCommentList.newInstance(params));

    }
}
