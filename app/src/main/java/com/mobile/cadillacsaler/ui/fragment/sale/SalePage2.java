package com.mobile.cadillacsaler.ui.fragment.sale;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.entity.sale.SaleItemData;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.net.SelfArrangeBean;
import com.mobile.cadillacsaler.widget.MyTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by chanc on 2018/6/13.
 */

public class SalePage2 extends LinearLayout {

    @BindView(R.id.bg)
    ImageView bg;
    Sale sale;
    ArrayList<SelfArrangeBean> items;
    @BindView(R.id.tv1)
    MyTextView mTv1;
    @BindView(R.id.tv2)
    MyTextView mTv2;
    @BindView(R.id.tv3)
    MyTextView mTv3;
    @BindView(R.id.tv4)
    MyTextView mTv4;

    public SalePage2(Context context) {
        super(context);
        init();
    }

    public SalePage2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setItems(ArrayList<SelfArrangeBean> items) {
        Glide.with(sale)
                .load(new File(Const.BG_PATH))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(bg);
        this.items = items;
        initItems();
        for (SelfArrangeBean item : items) {
            if ("1".equals(item.valid)) {
                MyTextView tv = list.removeFirst();
                tv.setVisibility(VISIBLE);
                tv.setText(item.name);
                tv.setTag(item);
            }
        }
    }

    LinkedList<MyTextView> list = new LinkedList<>();

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.self_arrange_list, this, true);
        ButterKnife.bind(this);


        initItems();
    }

    private void initItems() {
        mTv1.setVisibility(GONE);
        mTv2.setVisibility(GONE);
        mTv3.setVisibility(GONE);
        mTv4.setVisibility(GONE);
        list.addLast(mTv1);
        list.addLast(mTv2);
        list.addLast(mTv3);
        list.addLast(mTv4);
    }


    @OnClick({R.id.tv1, R.id.tv2, R.id.tv3, R.id.tv4})
    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.tv1:
//                break;
//            case R.id.tv2:
//                break;
//            case R.id.tv3:
//                break;
//            case R.id.tv4:
//                break;
//        }
        enterSelfList((SelfArrangeBean) view.getTag());
    }

    private void enterSelfList(SelfArrangeBean bean) {
        String menustxt = bean.menustxt;
        String[] split = menustxt.split(",");
        String[] items = new String[split.length];
        for (int i = 0; i < split.length; i++) {
            items[i] = SaleItemData.getItemName(Integer.parseInt(split[i]));
        }

        sale.start(SaleContent.newInstance(
                bean.name,
                items
        ));
    }

}
