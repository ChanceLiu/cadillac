package com.mobile.cadillacsaler.ui.fragment.start;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.Content;
import com.mobile.cadillacsaler.ui.fragment.SaleLogin;
import com.mobile.cadillacsaler.ui.fragment.SecondPageFragment;
import com.mobile.cadillacsaler.ui.fragment.practice.Practice;
import com.mobile.cadillacsaler.ui.fragment.practice.SelfLearn;
import com.mobile.cadillacsaler.ui.fragment.sale.Sale;
import com.mobile.cadillacsaler.ui.fragment.weapon.Weapon;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class Start extends BaseBackFragment {
    public ArrayList<Content> selfInpro;
    public ArrayList<Content> sellProduct;
    public ArrayList<Content> splendid;
    private String selfInproHistory="本品培训";
    private String sellProductHistory="三大卖点";
    private String splendidHistory="品牌风范";

    {
        selfInpro = new ArrayList<>();
        selfInpro.add(new Content("本品培训",R.mipmap.practice_btn_0));
        selfInpro.add(new Content("培训视频",R.mipmap.practice_btn_1));
        selfInpro.add(new Content("巩固测试",R.mipmap.practice_btn_2));
        sellProduct = new ArrayList<>();
        sellProduct.add(new Content("想要了解产品",R.mipmap.launch_1));
        sellProduct.add(new Content("已有意向竞品",R.mipmap.launch_1));
        sellProduct.add(new Content("关注价格优惠",R.mipmap.launch_1));
        sellProduct.add(new Content("犹豫高低车款",R.mipmap.launch_1));
        sellProduct.add(new Content("个性定制",R.mipmap.launch_1));

        splendid = new ArrayList<>();
        splendid.add(new Content("AR赏车",R.mipmap.launch_1));
        splendid.add(new Content("最新市价",R.mipmap.launch_1));
        splendid.add(new Content("品牌风范",R.mipmap.launch_1));
    }

    public static final String TAG = Start.class.getSimpleName();
    @BindView(R.id.cv_self_impro)
    RelativeLayout cvSelfImpro;
    @BindView(R.id.cv_sell_product)
    RelativeLayout cvSellProduct;
    @BindView(R.id.cv_splendid)
    RelativeLayout cvSplendid;


    public static Start newInstance() {
        Start fragment = new Start();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);
        initView(view);
        return view;
    }

    private void initView(View view) {
        ((MainActivity) _mActivity).showImgAccount(true);
        initItem(cvSelfImpro, "自我提升","上次浏览到"+selfInproHistory,R.drawable.start_btn_0);
        initItem(cvSellProduct, "本品培训","上次浏览到"+sellProductHistory,R.drawable.start_btn_1);
        initItem(cvSplendid, "精彩加持","上次浏览到"+splendidHistory,R.drawable.start_btn_2);
    }

    public void initItem(View view,String title,String history,int img) {
        ((TextView)view.findViewById(R.id.history)).setText(history);
        ((TextView)view.findViewById(R.id.tv_title)).setText(title);
        ((ImageView)view.findViewById(R.id.item)).setImageResource(img);
    }


    @OnClick({R.id.cv_self_impro, R.id.cv_sell_product, R.id.cv_splendid})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cv_self_impro:
                start(Practice.newInstance("自我提升",selfInpro));
                break;
            case R.id.cv_sell_product:
                start(Sale.newInstance("产品销售",sellProduct));
                break;
            case R.id.cv_splendid:
                start(Weapon.newInstance("精彩加持",splendid));
//                start(SaleLogin.newInstance());
                break;
        }
    }

    @Override
    public boolean onBackPressedSupport() {
//        popTo(Launch.class,true);

        _mActivity.finish();
        return super.onBackPressedSupport();
    }
}
