package com.mobile.cadillacsaler.ui.fragment.user;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.SelfLearnAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.practice.SelfLearnBean;
import com.mobile.cadillacsaler.ui.fragment.practice.SelfLearnDetail;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.vise.xsnow.util.VersionUtil;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 我的设置 设置背景
 */
public class MySetting extends BaseBackFragment implements SelfLearnAdapter.OnItemClickListener {

    public static final String TAG = MySetting.class.getSimpleName();
    private static final String ARG_CAR_TYPE = "arg_car_type";
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.version)
    MyTextView mVersion;


    public static MySetting newInstance() {
        MySetting fragment = new MySetting();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_setting, container, false);

        ButterKnife.bind(this, view);
        initView(view);


        return view;
    }


    private void initView(View view) {

        tvToolbarTitle.setText("我的设置");

        mVersion.setText("版本 " + VersionUtil.getLocalVersionName(_mActivity));


    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }


    @Override
    public void onItemClick(int position, SelfLearnBean data, View view) {
        start(SelfLearnDetail.newInstance(data));
    }

    @OnClick({R.id.custom_bg, R.id.version})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.custom_bg:
                ((MainActivity)_mActivity).useCamera();
                break;

        }
    }





}
