package com.mobile.cadillacsaler.ui.fragment.start;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.ui.fragment.SaleLogin;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class Launch extends BaseBackFragment {

    public static final String TAG = Launch.class.getSimpleName();
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.btnEnter)
    ImageButton btnEnter;
    @BindView(R.id.coord)
    FrameLayout coord;
    @BindView(R.id.dot)
    LinearLayout dot;
    @BindView(R.id.viewPagerContainer)
    FrameLayout viewPagerContainer;


    public static Launch newInstance() {
        Launch fragment = new Launch();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);

        ButterKnife.bind(this, view);
        initView(view);
        return view;
    }

    public void initDot() {
        View.inflate(_mActivity, R.layout.dot, dot);
        View.inflate(_mActivity, R.layout.dot, dot);
        View.inflate(_mActivity, R.layout.dot, dot);
        selectDot(0);
    }

    public void selectDot(int position) {
        for (int i = 0; i < dot.getChildCount(); i++) {
            dot.getChildAt(i).setSelected(false);
        }
        dot.getChildAt(position).setSelected(true);
    }

    private void initView(View view) {
        initDot();
        ((MainActivity) _mActivity).showImgAccount(false);
        viewPager.setAdapter(new MyPagerAdapter(this));
        viewPagerContainer.setVisibility(View.GONE);
        //5秒后输出 hello world , 然后显示一张图片
        Observable.timer(2, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        viewPagerContainer.setVisibility(View.VISIBLE);
                    }
                });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                btnEnter.setVisibility(position == 2 ? View.VISIBLE : View.GONE);
                selectDot(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    /**
     * 这里演示:
     * 比较复杂的Fragment页面会在第一次start时,导致动画卡顿
     * Fragmentation提供了onEnterAnimationEnd()方法,该方法会在 入栈动画 结束时回调
     * 所以在onCreateView进行一些简单的View初始化(比如 toolbar设置标题,返回按钮; 显示加载数据的进度条等),
     * 然后在onEnterAnimationEnd()方法里进行 复杂的耗时的初始化 (比如FragmentPagerAdapter的初始化 加载数据等)
     */


    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btnEnter)
    public void onClick() {
        String str = (String) SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "");
        if (TextUtils.isEmpty(str)) {
            startWithPop(SaleLogin.newInstance());
        } else {
            startWithPop(Start.newInstance());
        }

    }


    private static class MyPagerAdapter extends PagerAdapter {
        int[] launcherPage = {R.mipmap.launch_0, R.mipmap.launch_1, R.mipmap.launch_2};
        Launch fragment;

        MyPagerAdapter(Launch fragment) {
            this.fragment = fragment;
        }

        @Override
        public int getCount() {
            return launcherPage.length;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
//            ImageView view = new ImageView(fragment._mActivity);

            ImageView view = (ImageView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.item_view_pager, container, false);
            Glide.with(fragment)
                    .load(launcherPage[position])
                    .fitCenter()
                    .into(view);
            container.addView(view);
            return view;
        }

        private void flate(FragmentActivity mActivity, int item_view_pager, ViewGroup container) {
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }
}
