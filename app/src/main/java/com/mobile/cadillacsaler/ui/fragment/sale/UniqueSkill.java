package com.mobile.cadillacsaler.ui.fragment.sale;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.MainActivity;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.sale.SkillData;
import com.mobile.cadillacsaler.entity.sale.SkillDataBean;
import com.mobile.cadillacsaler.widget.LongPicView;

import java.io.File;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class UniqueSkill extends BaseBackFragment implements View.OnClickListener {


    @BindView(R.id.image)
    ImageView mImage;
    @BindView(R.id.container)
    LinearLayout mContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.btn_close)
    ImageButton mBtnClose;
    @BindView(R.id.btn_share)
    ImageButton mBtnShare;
    private SkillDataBean skillData;

    public static UniqueSkill newInstance() {
        UniqueSkill fragment = new UniqueSkill();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_uniqueskill, container, false);

        ButterKnife.bind(this, view);

        initView(view);
        return view;
    }


    private void initView(View view) {
        skillData = SkillData.getSkillData(CarType.currentType);
        Glide.with(this)
                .load(new File(skillData.bigImage))
                .into(mImage);

        mContainer.addView(getImageView(10, skillData.keys.get(0)));
        mContainer.addView(getImageView(11, skillData.getItemImage(skillData.keys.get(0))));
        mContainer.addView(getImageView(20, skillData.keys.get(1)));
        mContainer.addView(getImageView(21, skillData.getItemImage(skillData.keys.get(1))));
        mContainer.addView(getImageView(30, skillData.keys.get(2)));
        mContainer.addView(getImageView(31, skillData.getItemImage(skillData.keys.get(2))));

        for (int i = 0; i < mContainer.getChildCount(); i++) {
            View v = mContainer.getChildAt(i);
            if (v instanceof ImageView) {
                v.setOnClickListener(this);
            }

        }

    }


    public ImageView getImageView(int id, String imgSrc) {
        ImageView imgView = (ImageView) LayoutInflater.from(_mActivity).inflate(R.layout.skill_img_item, mContainer, false);
        imgView.setId(id);
        Glide.with(this)
                .load(new File(imgSrc))
                .into(imgView);
        if (id == 11 || id == 21 || id == 31) {
            imgView.setVisibility(View.GONE);
        }
        return imgView;
    }

    @Override
    public void onSupportVisible() {
        super.onSupportVisible();
        ((MainActivity) _mActivity).showImgAccount(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) _mActivity).showImgAccount(true);

    }


    @OnClick(R.id.btn_close)
    public void onClick() {
        pop();
    }

    @Override
    public void onClick(View v) {
        int iid = -1;
        switch (v.getId()) {
            case 10:
                iid = 11;
                break;
            case 20:
                iid = 21;
                break;
            case 30:
                iid = 31;
                break;
        }

        final View view = mContainer.findViewById(iid);
        if (view != null) {
            view.setVisibility(view.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
            //                    mScrollView.smoothScrollTo(0, view.getHeight());

        }
    }
}
