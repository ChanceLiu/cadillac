package com.mobile.cadillacsaler.ui.fragment.sale;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.xrecyclerview.XRecyclerView;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.adapter.PromotionAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.mobile.cadillacsaler.net.PromotionBean;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * 综合报价
 */
public class SalePomotionMoreGifts extends BaseBackFragment {


    @BindView(R.id.iv_toolbar_left)
    ImageView mIvToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView mIvToolbarLogo;
    @BindView(R.id.tv_left)
    MyTextView mTvLeft;
    @BindView(R.id.tv_toolbar_title)
    MyTextView mTvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView mIvToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.recy)
    XRecyclerView mRecy;


    private List<PromotionBean> data = new ArrayList<>();
    private PromotionAdapter mAdapter;
    private boolean isRefresh;
    private boolean isLoad;
    private int currentPage;


    public static SalePomotionMoreGifts newInstance() {
        SalePomotionMoreGifts fragment = new SalePomotionMoreGifts();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sale_pomotion_more_gifts, container, false);

        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        initView(view);
        return view;
    }


    private void initView(View view) {
        mTvToolbarTitle.setTextColor(Color.GRAY);
        mTvToolbarTitle.setText("更多优惠");
        initRecycleView();
    }

    @Override
    public void onSupportVisible() {
        super.onSupportVisible();

        currentPage = 0;
        getNetData(currentPage + "", CarType.currentType + "");

    }

    public void getNetData(String page, String cartype) {
        Map<String, String> params = new HashMap<>();
        params.put("action", "getNews");
        params.put("page", page);
        params.put("type", "更多赠送");
        params.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");
        params.put("cartype", cartype);

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, ArrayList<PromotionBean>>() {
                    @Override
                    public ArrayList<PromotionBean> apply(Map<String, String> params) throws Exception {

                        NetBean bean = NetUtil.getNetData(params, PromotionBean.class, true);

                        ArrayList<Map<Object, Object>> data = (ArrayList<Map<Object, Object>>) bean.data;
                        ArrayList<PromotionBean> items = new ArrayList<>();
                        try {
                            for (Map<Object, Object> map : data) {
                                PromotionBean o = NetUtil.mapToObject(map, PromotionBean.class);
                                items.add(o);
                            }
                        } catch (Exception e) {

                        }

                        return items;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ArrayList<PromotionBean>>() {
                    @Override
                    public void accept(ArrayList<PromotionBean> userBean) {
                        if (isRefresh) {
                            data.clear();
                            data.addAll(userBean);
                        } else {
                            data.addAll(userBean);
                        }
                        mAdapter.notifyDataSetChanged();
                        mRecy.refreshComplete();
                        mRecy.noMoreLoading();
                    }
                });

    }


    private void initRecycleView() {
        mAdapter = new PromotionAdapter(this, data);
        final LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);


        mRecy.setLoadingMoreEnabled(true);
        mRecy.setPullRefreshEnabled(true);

        mRecy.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
                isLoad = false;
                currentPage = 0;
                getNetData(currentPage + "", CarType.currentType + "");
            }

            @Override
            public void onLoadMore() {
                isRefresh = false;
                isLoad = true;
                getNetData(++currentPage + "", CarType.currentType + "");

            }
        });


    }


    @OnClick(R.id.iv_toolbar_left)
    public void back() {
        pop();
    }


}
