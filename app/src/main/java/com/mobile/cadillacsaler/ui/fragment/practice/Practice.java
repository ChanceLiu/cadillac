package com.mobile.cadillacsaler.ui.fragment.practice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.SecondPageAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.Content;
import com.mobile.cadillacsaler.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class Practice extends BaseBackFragment {
    @BindView(R.id.iv_car_type)
    ImageView mIvCarType;
    @BindView(R.id.item1)
    ImageView mItem1;
    @BindView(R.id.item2)
    ImageView mItem2;
    @BindView(R.id.item3)
    ImageView mItem3;


    public static final String TAG = Practice.class.getSimpleName();
    private static final String ARG_TITLE = "arg_title";
    private static final String ARG_ITEMS = "arg_items";
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    private String mTitle;
    private ArrayList<Content> mItems;

    public static Practice newInstance(String title, ArrayList<Content> items) {
        Practice fragment = new Practice();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        bundle.putSerializable(ARG_ITEMS, items);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_practice, container, false);

        ButterKnife.bind(this, view);
        initView(view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = bundle.getString(ARG_TITLE);
            mItems = (ArrayList<Content>) bundle.getSerializable(ARG_ITEMS);
        }
        return view;
    }


    private void initView(View view) {
        mIvCarType.setImageResource(CarType.getCarTypeIcon());
        tvToolbarTitle.setText("自我提升");
        initToolbarNav(toolbar);
    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }


    @OnClick({R.id.item1, R.id.item2, R.id.item3})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.item1:
                start(SelfLearn.newInstance(CarType.currentType));
                break;
            case R.id.item2:
                start(TrainVideo.newInstance());
                break;
            case R.id.item3:
                start(Exam.newInstance());
                break;
        }
    }
}
