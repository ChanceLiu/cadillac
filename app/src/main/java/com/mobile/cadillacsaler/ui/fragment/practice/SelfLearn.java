package com.mobile.cadillacsaler.ui.fragment.practice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.adapter.SelfLearnAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.practice.SelfLearnBean;
import com.mobile.cadillacsaler.entity.practice.SelfLearnData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class SelfLearn extends BaseBackFragment implements SelfLearnAdapter.OnItemClickListener {
//    public static final int ats_l = 0;
//    public static final int xts = 1;
//    public static final int ct6 = 2;
//    public static final int xt5 = 3;

    public static final String TAG = SelfLearn.class.getSimpleName();
    private static final String ARG_CAR_TYPE = "arg_car_type";
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recy)
    RecyclerView mRecy;
    @BindView(R.id.tv_item_0)
    TextView mTvItem0;
    @BindView(R.id.tv_item_1)
    TextView mTvItem1;
    @BindView(R.id.tv_item_2)
    TextView mTvItem2;
    @BindView(R.id.tv_item_3)
    TextView mTvItem3;


    private SelfLearnAdapter mAdapter;
    private int carType;
    private SelfLearnData data;


    public static SelfLearn newInstance(int carType) {
        SelfLearn fragment = new SelfLearn();
        Bundle bundle = new Bundle();

        bundle.putInt(ARG_CAR_TYPE, carType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_self_learn, container, false);

        ButterKnife.bind(this, view);
        initView(view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            carType = bundle.getInt(ARG_CAR_TYPE, 0);
        }
        setTitle(carType);
        initData(carType);
        mAdapter.setOnItemClickListener(this);

        return view;
    }

    private void setTitle(int carType) {

        switch (carType) {
            case CarType.ats_l:
                tvToolbarTitle.setText("ATS-L本品培训");
                break;
            case CarType.xts:
                tvToolbarTitle.setText("XTS本品培训");
                break;
            case CarType.ct6:
                tvToolbarTitle.setText("CT6本品培训");
                break;
            case CarType.xt5:
                tvToolbarTitle.setText("XT5本品培训");
                break;
        }
    }

    private void initData(int carType) {
        data = new SelfLearnData(carType);
        mAdapter.setDatas(data.mItems1, data.mItems2, data.mItems3);


    }


    private void initView(View view) {


        initToolbarNav(toolbar);

        mAdapter = new SelfLearnAdapter(this);
        LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.addItemDecoration(new DividerItemDecoration(_mActivity, DividerItemDecoration.VERTICAL));
        /** * 既然是动画，就会有时间，我们把动画执行时间变大一点来看一看效果 */
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setAddDuration(200);
        defaultItemAnimator.setRemoveDuration(200);
        mRecy.setItemAnimator(defaultItemAnimator);


        mRecy.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
        mTvItem0.setSelected(true);
    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }


    @Override
    public void onItemClick(int position, SelfLearnBean data, View view) {
        start(SelfLearnDetail.newInstance(data));
    }

    @OnClick({R.id.tv_item_0, R.id.tv_item_1, R.id.tv_item_2, R.id.tv_item_3})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_item_0:
                mAdapter.setDatas(data.mItems1, data.mItems2, data.mItems3);
                break;
            case R.id.tv_item_1:
                mAdapter.setDatas(data.mItems1);
                break;
            case R.id.tv_item_2:
                mAdapter.setDatas(data.mItems2);
                break;
            case R.id.tv_item_3:
                mAdapter.setDatas(data.mItems3);
                break;
        }
        selectItem(view);
    }

    public void selectItem(View view) {
        mTvItem0.setSelected(false);
        mTvItem1.setSelected(false);
        mTvItem2.setSelected(false);
        mTvItem3.setSelected(false);
        view.setSelected(true);

    }
}
