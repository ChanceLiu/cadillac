package com.mobile.cadillacsaler.ui.fragment.practice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.widget.MyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class Exam extends BaseBackFragment {

    private static final String ARG_ITEMS = "arg_items";
    @BindView(R.id.iv_toolbar_left)
    ImageView mIvToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView mIvToolbarLogo;
    @BindView(R.id.tv_left)
    MyTextView mTvLeft;
    @BindView(R.id.tv_toolbar_title)
    MyTextView mTvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView mIvToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static Exam newInstance() {
        Exam fragment = new Exam();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exam, container, false);

        ButterKnife.bind(this, view);

        initView(view);
        return view;
    }


    private void initView(View view) {

        mTvToolbarTitle.setText("巩固测试");

    }

    @OnClick({R.id.iv_toolbar_left,  R.id.cv_item_1, R.id.cv_item_2})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_toolbar_left:
                pop();
                break;
            case R.id.cv_item_1:
                start(OnLineExam.newInstance());
                break;
            case R.id.cv_item_2:
                Toast.makeText(_mActivity, "正在开发中...", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
