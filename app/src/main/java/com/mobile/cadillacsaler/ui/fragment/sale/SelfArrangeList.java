package com.mobile.cadillacsaler.ui.fragment.sale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.net.KeHuXinXiBean;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * 自定义画面列表  selfArrange文件夹
 */
public class SelfArrangeList extends BaseBackFragment {

    public static final String TAG = SelfArrangeList.class.getSimpleName();
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    LinearLayout mContainer;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;


    public static SelfArrangeList newInstance() {
        SelfArrangeList fragment = new SelfArrangeList();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liaojiechanpin, container, false);

        ButterKnife.bind(this, view);
        initView(view);
        return view;
    }


    private void initView(View view) {

        tvToolbarTitle.setText("犹豫高低车款");

    }




    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }





}
