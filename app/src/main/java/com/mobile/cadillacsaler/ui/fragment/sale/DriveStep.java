package com.mobile.cadillacsaler.ui.fragment.sale;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.sale.DriveStepData;
import com.mobile.cadillacsaler.widget.LongPicView;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.zlc.video.utils.ScreenUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 试乘试驾
 */
public class DriveStep extends BaseBackFragment {


    @BindView(R.id.videoview)
    ImageView mVideoview;
    @BindView(R.id.iv_start)
    ImageView mIvStart;
    @BindView(R.id.iv_toolbar_left)
    ImageView mIvToolbarLeft;
    @BindView(R.id.ivHandle)
    ImageView mIvHandle;
    @BindView(R.id.btnTryDrive)
    ImageButton mBtnTryDrive;
    @BindView(R.id.btnTrySit)
    ImageButton mBtnTrySit;

    @BindView(R.id.imgLong)
    LongPicView mImgLong;

    @BindView(R.id.container)
    FrameLayout mContainer;
    @BindView(R.id.selector)
    LinearLayout mSelector;
    @BindView(R.id.tv_toolbar_title)
    MyTextView mTvToolbarTitle;

    public static DriveStep newInstance() {
        DriveStep fragment = new DriveStep();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_step, container, false);

        ButterKnife.bind(this, view);
        initView(view);
        return view;
    }


    private void initView(View view) {
        mTvToolbarTitle.setText("试乘试驾");
        mTvToolbarTitle.setTextColor(Color.GRAY);
        //开始未选中
//        Glide.with(this)
//                .load(DriveStepData.getImageFile(CarType.currentType))
//                .into(mImgLong);
        mContainer.setVisibility(View.GONE);
    }


    @OnClick({R.id.iv_start, R.id.iv_toolbar_left, R.id.ivHandle, R.id.btnTryDrive, R.id.btnTrySit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_start:
                //下面android.resource://是固定的，com.example.work是包名，R.raw.sw是你raw文件夹下的视频文件
                Glide.with(this)
                        .load(R.mipmap.drive_step_action)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mVideoview);

                mContainer.setVisibility(View.VISIBLE);
                mIvHandle.setVisibility(View.VISIBLE);

                ViewCompat.animate(mContainer)
                        .translationY(mContainer.getHeight())
                        .setInterpolator(new AccelerateInterpolator(1))
                        .setDuration(300)
                        .start();

                break;
            case R.id.iv_toolbar_left:
                pop();
                break;
            case R.id.ivHandle:
                view.setSelected(!view.isSelected());
                mImgLong.setVisibility(View.VISIBLE);
                int screenHeight = ScreenUtil.getScreenHeight(_mActivity);
                ViewGroup.LayoutParams p = mImgLong.getLayoutParams();
                p.height = screenHeight / 4 * 3;
                mImgLong.setLayoutParams(p);
                mImgLong.setImageSource(DriveStepData.getImageFile(CarType.currentType).getAbsolutePath());


                mSelector.setVisibility(mSelector.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);


                break;
            case R.id.btnTryDrive:

                if (!view.isSelected()) {
                    select(view);
                }
                break;
            case R.id.btnTrySit:

                if (!view.isSelected()) {
                    select(view);
                }
                break;
        }
    }


    private void select(View view) {
        mBtnTryDrive.setSelected(false);
        mBtnTrySit.setSelected(false);
        view.setSelected(true);
    }
}
