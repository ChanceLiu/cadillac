package com.mobile.cadillacsaler.ui.fragment.sale;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.entity.CarType;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by chanc on 2018/6/13.
 */

public class SalePage1 extends LinearLayout {

    @BindView(R.id.iv_car_type)
    ImageView mIvCarType;
    Sale sale;

    public SalePage1(Context context) {
        super(context);
        init();
    }

    public SalePage1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.sale_page_1, this, true);
        ButterKnife.bind(this);
        mIvCarType.setImageResource(CarType.getCarTypeIcon());
    }


    @OnClick({R.id.item1, R.id.item2, R.id.item3, R.id.item4, R.id.item5})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.item1:
                sale.start(SaleContent.newInstance(
                        "想要了解产品",
                        "车型亮点",
                        "车款配置",
                        "自我对比",
                        "试乘试驾",
                        "综合报价"
                ));
                break;
            case R.id.item2:
                sale.start(SaleContent.newInstance(
                        "已有意向竞品",
                        "竞品对比",
                        "网络口碑",
                        "三大卖点",
                        "车款配置",
                        "试乘试驾",
                        "综合报价"
                ));


                break;
            case R.id.item3:
                sale.start(SaleContent.newInstance(
                        "关注价格优惠",
                        "综合报价",
                        "自我对比",
                        "车款配置",
                        "竞品对比",
                        "试乘试驾"

                ));
//                start(JiaGeYouHui.newInstance());
                break;
            case R.id.item4:

                sale.start(SaleContent.newInstance(
                        "犹豫高低车款",
                        "自我对比",
                        "车款配置",
                        "网络口碑",
                        "试乘试驾",
                        "综合报价"
                ));

//                start(CheKuan.newInstance());
                break;
            case R.id.item5:
                sale.start(SelfArrange.newInstance());

                break;
        }
    }


}
