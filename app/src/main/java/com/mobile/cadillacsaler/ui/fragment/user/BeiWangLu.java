package com.mobile.cadillacsaler.ui.fragment.user;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xrecyclerview.XRecyclerView;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.SpKey;
import com.mobile.cadillacsaler.adapter.BeiWangLuAdapter;
import com.mobile.cadillacsaler.adapter.SelfLearnAdapter;
import com.mobile.cadillacsaler.base.BaseBackFragment;
import com.mobile.cadillacsaler.entity.practice.SelfLearnBean;
import com.mobile.cadillacsaler.net.BeiWangLuBean;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.net.NetBean;
import com.mobile.cadillacsaler.net.NetUtil;
import com.mobile.cadillacsaler.ui.fragment.practice.SelfLearnDetail;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.vise.utils.assist.DateUtil;
import com.vise.xsnow.util.SharePreferenceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by YoKeyword on 16/2/3.
 */
public class BeiWangLu extends BaseBackFragment implements SelfLearnAdapter.OnItemClickListener {

    public static final String TAG = BeiWangLu.class.getSimpleName();
    private static final String ARG_CAR_TYPE = "arg_car_type";
    @BindView(R.id.iv_toolbar_left)
    ImageView ivToolbarLeft;
    @BindView(R.id.iv_toolbar_logo)
    ImageView ivToolbarLogo;
    @BindView(R.id.tv_left)
    TextView tvLeft;

    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.iv_toolbar_right)
    ImageView ivToolbarRight;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recy)
    XRecyclerView mRecy;
    @BindView(R.id.tv_toolbar_right)
    MyTextView mTvToolbarRight;


    private List<BeiWangLuBean> data = new ArrayList<>();
    private BeiWangLuAdapter mAdapter;
    private boolean isRefresh;
    private boolean isLoad;
    private int currentPage;
    private long time;
    private int mYear;
    private int mMonth;
    private int mDay;
    private String selectTime;


    public static BeiWangLu newInstance() {
        BeiWangLu fragment = new BeiWangLu();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_beiwanglu, container, false);

        ButterKnife.bind(this, view);
        initView(view);


        return view;
    }

    private void initView(View view) {
        tvToolbarTitle.setText("备忘录");
        mTvToolbarRight.setVisibility(View.VISIBLE);


        initRecycleView();
        time = System.currentTimeMillis();


    }

    @Override
    public void onSupportVisible() {
        super.onSupportVisible();
        currentPage = 0;
        isRefresh = true;
        getNetData(currentPage + "", time);
    }

    public void getNetData(String page, long time) {
        String t = DateUtil.getStringByFormat(new Date(time), new SimpleDateFormat(Const.DATE_FORMAT2));
        getNetData(page, t);
    }

    public void getNetData(String page, String time) {
        selectTime = time;

        Map<String, String> params = new HashMap<>();
        params.put("action", Const.action_getMessage);
        params.put("page", page);
        params.put("times", time);
        params.put("uid", SharePreferenceUtil.getParam(_mActivity, SpKey.KEY_UID, "") + "");

        Observable.fromArray(params)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, String>, ArrayList<BeiWangLuBean>>() {
                    @Override
                    public ArrayList<BeiWangLuBean> apply(Map<String, String> params) throws Exception {

                        NetBean bean = NetUtil.getNetData(params, BeiWangLuBean.class, true);

                        ArrayList<Map<Object, Object>> data = (ArrayList<Map<Object, Object>>) bean.data;
                        ArrayList<BeiWangLuBean> items = new ArrayList<>();
                        try {
                            for (Map<Object, Object> map : data) {
                                BeiWangLuBean o = NetUtil.mapToObject(map, BeiWangLuBean.class);
                                items.add(o);
                            }
                        } catch (Exception e) {

                        }

                        return items;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ArrayList<BeiWangLuBean>>() {
                    @Override
                    public void accept(ArrayList<BeiWangLuBean> userBean) {
                        if (isRefresh) {
                            mRecy.refreshComplete();
                            data.clear();
                            data.addAll(userBean);
                            mAdapter.notifyDataSetChanged();
                        }
                        if (isLoad) {
                            data.addAll(userBean);
                            mAdapter.notifyDataSetChanged();
                        }
                        mRecy.noMoreLoading();
                    }
                });

    }


    private void initRecycleView() {
        mAdapter = new BeiWangLuAdapter(this, data);
        final LinearLayoutManager manager = new LinearLayoutManager(_mActivity);
        mRecy.setLayoutManager(manager);
        mRecy.setAdapter(mAdapter);


        mRecy.setLoadingMoreEnabled(true);
        mRecy.setPullRefreshEnabled(true);

        mRecy.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
                isLoad = false;
                currentPage = 0;
                getNetData(currentPage + "", selectTime);
            }

            @Override
            public void onLoadMore() {
                isRefresh = false;
                isLoad = true;
                getNetData(++currentPage + "", selectTime);

            }
        });


    }


    @OnClick(R.id.iv_toolbar_left)
    public void onClick() {
        pop();
    }


    @Override
    public void onItemClick(int position, SelfLearnBean data, View view) {
        start(SelfLearnDetail.newInstance(data));
    }

    @OnClick(R.id.add)
    public void add() {
        start(AddBeiWangLu.newInstance());
    }

    @SuppressLint("WrongConstant")
    @OnClick(R.id.tv_toolbar_right)
    public void choseDate() {

        Calendar ca = Calendar.getInstance();
        mYear = ca.get(Calendar.YEAR);
        mMonth = ca.get(Calendar.MONTH);
        mDay = ca.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(_mActivity, onDateSetListener, mYear, mMonth, mDay).show();

    }


    /**
     * 日期选择器对话框监听
     */
    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            String days;
            if (mMonth + 1 < 10) {
                if (mDay < 10) {
                    days = new StringBuffer().append(mYear).append("-").append("0").
                            append(mMonth + 1).append("-").append("0").append(mDay).toString();
                } else {
                    days = new StringBuffer().append(mYear).append("-").append("0").
                            append(mMonth + 1).append("-").append(mDay).toString();
                }

            } else {
                if (mDay < 10) {
                    days = new StringBuffer().append(mYear).append("-").
                            append(mMonth + 1).append("-").append("0").append(mDay).toString();
                } else {
                    days = new StringBuffer().append(mYear).append("-").
                            append(mMonth + 1).append("-").append(mDay).toString();
                }

            }
            currentPage = 0;
            isRefresh = true;
            getNetData(currentPage+"", days);
        }
    };

}
