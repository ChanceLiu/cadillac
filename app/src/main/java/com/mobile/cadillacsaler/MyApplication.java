package com.mobile.cadillacsaler;

import android.app.Application;

import com.mob.MobSDK;
import com.mobile.cadillacsaler.entity.CarType;
import com.vise.xsnow.util.SharePreferenceUtil;

/**
 * Created by chanc on 2018/6/8.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MobSDK.init(this);

        CarType.currentType = (int) SharePreferenceUtil.getParam(this, SpKey.KEY_CAR_TYPE, 0);
    }
}
