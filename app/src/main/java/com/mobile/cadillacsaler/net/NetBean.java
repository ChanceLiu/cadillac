package com.mobile.cadillacsaler.net;

/**
 * Created by chanc on 2018/6/11.
 */

public class NetBean<T> {
    /**
     * code : 200
     * msg : OK
     * data :
     */

    public int code;
    public String msg;
    public T data;

    public NetBean() {

    }

    public NetBean(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

}
