package com.mobile.cadillacsaler.net;

/**
 * Created by chanc on 2018/6/11.
 */

public class UserBean {
    /**
     * uid : 25723
     * tel : CD1000
     * nickname : 上海东昌
     * headimg :
     * logintime : 1528672022
     * account : CD1000
     */

    public String uid;
    public String tel;
    public String nickname;
    public String headimg;
    public String logintime;
    public String account;




    @Override
    public String toString() {
        return "UserBean{" +
                "uid=" + uid +
                ", tel='" + tel + '\'' +
                ", nickname='" + nickname + '\'' +
                ", headimg='" + headimg + '\'' +
                ", logintime=" + logintime +
                ", account='" + account + '\'' +
                '}';
    }
}
