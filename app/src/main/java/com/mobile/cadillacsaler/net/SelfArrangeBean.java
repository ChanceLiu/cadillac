package com.mobile.cadillacsaler.net;

import java.io.Serializable;

/**
 * Created by chanc on 2018/6/13.
 */

public class SelfArrangeBean implements Serializable {
    /**
     * id : 5
     * uid : 25723
     * menustxt : 1,3,4
     * creattime : 1528816016
     * name : 122
     * valid : 1
     */

    public String id;
    public String uid;
    public String menustxt;
    public String creattime;
    public String name;
    public String valid;
}
