package com.mobile.cadillacsaler.net;

/**
 * Created by chanc on 2018/6/9.
 */

public interface Const {
    String baseUrl = "http://cadillac.sh-jinger.com/";
    String action_login = "login";
    String action_addLetter = "addLetter";
    String action_addMessage = "addMessage";
    String action_getMessage = "getMessage";
    String action_addCustMessage = "addCustMessage";
    String action_getCustMessage = "getCustMessage";
    String action_getAllMenus="getAllMenus";
    String action_addMenus="addMenus";
    String action_updateMenus="updateMenus";
    String action_setIndexMenus="setIndexMenus";
    String action_delMenus="delMenus";


    String action_delCustMessage = "delCustMessage";
    String BG_PATH = "/sdcard/CadillacSaler/background/bg.jpg";
    String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String DATE_FORMAT2 = "yyyy-MM-dd";
}

