package com.mobile.cadillacsaler.net;

import com.google.gson.reflect.TypeToken;
import com.vise.xsnow.common.GsonUtil;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by chanc on 2018/6/9.
 */

public class NetUtil {
    public static OkHttpClient client = new OkHttpClient.Builder().build();

    public static <T> NetBean<T> getNetData(Map<String, String> params, Class<T> clazz, boolean isList) throws IllegalAccessException, InstantiationException, IOException {

        FormBody.Builder formBodyBuid = new FormBody.Builder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String action = entry.getKey();
            String value = entry.getValue();
            formBodyBuid.add(action, value);
        }

        Request req = new Request.Builder()
                .url(Const.baseUrl)
                .post(formBodyBuid.build())
                .build();

        Response execute = client.newCall(req).execute();

        String json = execute.body().string();
        Type type;
        if (isList) {
            type = new TypeToken<NetBean<ArrayList<T>>>() {
            }.getType();
        } else {
            type = new TypeToken<NetBean<T>>() {
            }.getType();


        }


        NetBean<T> t = GsonUtil.gson().fromJson(json, type);
        return t;

    }

    /*
  * [Util dealNetAction:@"addLetter" param: @{@"content":_textView.text,@"contact":_textField.text}]
  * */
    public static void feedback(String content, String contact, String uid) {
        OkHttpClient client = new OkHttpClient.Builder().build();

        RequestBody body = new FormBody.Builder()
                .add("content", content)
                .add("contact", contact)
                .add("uid", uid)
                .add("action", Const.action_addLetter)
                .build();
        final Request request = new Request
                .Builder()
                .url(Const.baseUrl)
                .post(body).build();


        client
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        System.out.println();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String result = response.body().string();
                        System.out.println(result);
                    }
                });

    }

    public static<T> T mapToObject(Map<Object, Object> map, Class<T> beanClass) throws Exception {
        if (map == null)
            return null;
        T obj = beanClass.newInstance();
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            int mod = field.getModifiers();
            if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                continue;
            }
            field.setAccessible(true);
            field.set(obj, map.get(field.getName()) + "");
        }
        return obj;
    }
}
