package com.mobile.cadillacsaler.net;

import java.io.Serializable;

/**
 * Created by chanc on 2018/6/11.
 */

public class OnLineCommentBean implements Serializable {
    /**
     * id : 32
     * car_id : null
     * n_type : 动力
     * n_title : 驾驶者之车，凯迪拉克ATSL
     * n_descimg : http://p32cl168d.bkt.clouddn.com/152556343713815.png
     * n_desccontent : <p style="text-align: center;">
     <span style="color: red; font-family: 微软雅黑; text-align: justify; font-size: 36px;">驾驶者之车，凯迪拉克ATSL</span><br/>
     </p>
     <p style="text-align: right;">
     <span style="font-family: 微软雅黑; text-align: justify; font-size: 18px; color: rgb(0, 0, 0);">-<span style="font-family: 微软雅黑; text-align: justify; color: rgb(0, 0, 0); font-size: 14px;"><span style="text-align: justify; font-style: normal; font-family: 微软雅黑;">28T</span><span style="text-align: justify; font-family: 微软雅黑;">技术型用车感受</span></span></span>
     </p>
     <p style="text-align: right;">
     <span style="color: rgb(0, 0, 0); font-size: 14px; text-align: justify; font-family: 微软雅黑;"><br/></span>
     </p>
     <p style="text-align: right;">
     <span style="color: rgb(0, 0, 0); font-size: 14px; text-align: justify; font-family: 微软雅黑;"></span>
     </p>
     <p style="text-align: left; font-size: medium; font-family: Cambria; white-space: normal; line-height: 2em;">
     <span style="font-family: 微软雅黑; font-size: 18px;">【动力】</span>
     </p>
     <p style="text-align: left; font-size: medium; font-family: Cambria; white-space: normal; line-height: 2em;">
     <span style="font-family: 微软雅黑; font-size: 18px;">买这车的都是冲着动力的，主要谈谈动力。动力绝对够了，但是由于涡轮介入比较晚，变速箱也略显短板，所以动力来的有些突兀。我现在的驾驶习惯一般平顺为主，感觉就像个2.0的自吸车。安静平顺，但是这车又可以随时让你去激烈驾驶。宜静宜动。动力表现还是不错的，外加音浪补偿，激烈驾驶起来，在车内还是挺嗨的。感觉这车行车品质很不错，车子稳稳地，但是车速就很快了。</span>
     </p>
     <p style="text-align: left; font-size: medium; font-family: Cambria; white-space: normal; line-height: 2em;">
     <span style="font-family: 微软雅黑; font-size: 18px;">跑高速：跑了一次高速，行车品质很好，即安静提速又快，高速的油耗也控制的不错。120的时候转速只有2000。</span>
     </p>
     <p style="text-align: right;">
     <span style="color: rgb(0, 0, 0); font-size: 14px; text-align: justify; font-family: 微软雅黑;"><br/></span><br/>
     </p>
     * n_content : <p>
     <br/>
     </p><img src="http://image.xmyeditor.com/uploads/666/665272/20180506/a7e84bda57f3d98bf0585567b9993cde_thumb.jpg" title="驾驶者之车.jpg" alt="驾驶者之车.jpg"/>
     * n_carname : 0
     * n_carclassify : 2017款 ATS-L28T技术型
     * n_buytime : 2017-08
     * n_publictime : 2017年9月21日
     * n_price : 21.50万
     * n_store : 上海闵行凯迪拉克
     * n_area : 上海市
     * creattime : 1525563557
     * n_sort : 0
     * isdel : 0
     */

    public String id;
    public String car_id;
    public String n_type;
    public String n_title;
    public String n_descimg;
    public String n_desccontent;
    public String n_content;
    public String n_carname;
    public String n_carclassify;
    public String n_buytime;
    public String n_publictime;
    public String n_price;
    public String n_store;
    public String n_area;
    public String creattime;
    public String n_sort;
    public String isdel;

    @Override
    public String toString() {
        return "OnLineComment{" +
                "id='" + id + '\'' +
                ", car_id=" + car_id +
                ", n_type='" + n_type + '\'' +
                ", n_title='" + n_title + '\'' +
                ", n_descimg='" + n_descimg + '\'' +
                ", n_desccontent='" + n_desccontent + '\'' +
                ", n_content='" + n_content + '\'' +
                ", n_carname='" + n_carname + '\'' +
                ", n_carclassify='" + n_carclassify + '\'' +
                ", n_buytime='" + n_buytime + '\'' +
                ", n_publictime='" + n_publictime + '\'' +
                ", n_price='" + n_price + '\'' +
                ", n_store='" + n_store + '\'' +
                ", n_area='" + n_area + '\'' +
                ", creattime='" + creattime + '\'' +
                ", n_sort='" + n_sort + '\'' +
                ", isdel='" + isdel + '\'' +
                '}';
    }
}
