package com.mobile.cadillacsaler.net;

/**
 * Created by chanc on 2018/6/12.
 */

public class PromotionBean {

    /**
     * id : 22
     * n_carname : 2
     * n_type : 更多赠送
     * n_title : 彰显自我个性 测试凯迪拉克CT6 28T
     * n_descimg :
     * n_content :
     * creattime : 1528685045
     * n_sort : 10
     * isdel : 0
     * uid : 25723
     * org_id : 195
     */

    public String id;
    public String n_carname;
    public String n_type;
    public String n_title;
    public String n_descimg;
    public String n_content;
    public String creattime;
    public String n_sort;
    public String isdel;
    public String uid;
    public String org_id;
}
