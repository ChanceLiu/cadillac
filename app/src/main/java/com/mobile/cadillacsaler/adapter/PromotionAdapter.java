package com.mobile.cadillacsaler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.net.OnLineCommentBean;
import com.mobile.cadillacsaler.net.PromotionBean;
import com.mobile.cadillacsaler.ui.fragment.sale.CommonWebView;
import com.mobile.cadillacsaler.ui.fragment.sale.OnlineCommentDetail;
import com.mobile.cadillacsaler.widget.MyTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 主页HomeFragment  Adapter
 */
public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.MyViewHolder> {
    private List<PromotionBean> mItems = new ArrayList<>();
    private LayoutInflater mInflater;
    MySupportFragment fragment;
    Context context;


    public PromotionAdapter(MySupportFragment fragment, List<PromotionBean> data) {
        this(fragment._mActivity);
        this.fragment = fragment;
        mItems = data;

    }

    public PromotionAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setDatas(List<PromotionBean> items) {
        mItems.clear();
        mItems.addAll(items);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_promotion_list, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PromotionBean item = getItem(position);
        holder.bindData(item, position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public PromotionBean getItem(int position) {
        return mItems.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv1)
        MyTextView mTv1;
        @BindView(R.id.imageView)
        ImageView mImageView;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindData(final PromotionBean item, final int position) {

            Glide.with(fragment)
                    .load(item.n_descimg)
                    .placeholder(R.mipmap.cadi_icon)
                    .into(mImageView);


            mTv1.setText(item.n_title);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: 2018/6/13 换成具体链接
                    fragment.start(CommonWebView.newInstance("https://baidu.com","详情"));
                }
            });
        }
    }

}
