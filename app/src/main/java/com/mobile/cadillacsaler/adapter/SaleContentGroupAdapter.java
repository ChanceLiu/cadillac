package com.mobile.cadillacsaler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.entity.sale.SaleItemBean;
import com.mobile.cadillacsaler.entity.sale.SaleSubItemBean;
import com.tuacy.recyclerexpand.PatrolItem;
import com.tuacy.recyclerexpand.expand.ExpandGroupItemEntity;
import com.tuacy.recyclerexpand.expand.RecyclerExpandBaseAdapter;
import com.tuacy.recyclerexpand.utils.ResourceUtils;

import java.io.File;
import java.util.List;


public class SaleContentGroupAdapter extends RecyclerExpandBaseAdapter<SaleItemBean, SaleSubItemBean, RecyclerView.ViewHolder> {


    MySupportFragment fragment;
    private OnGroupItemClickListener onGroupClickListener;

    public void setOnGroupClickListener(OnGroupItemClickListener onGroupClickListener) {
        this.onGroupClickListener = onGroupClickListener;
    }

    public interface OnGroupItemClickListener{
        public void onGroupItemClickListener(int position, View view);
    }

    public SaleContentGroupAdapter(MySupportFragment fragment) {
        this.fragment = fragment;
    }

    public SaleContentGroupAdapter(List<ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>> dataList, MySupportFragment fragment) {
        super(dataList);
        this.fragment = fragment;
    }

    /**
     * 悬浮标题栏被点击的时候，展开收起切换功能
     */
    public void switchExpand(int adapterPosition) {
        int groupIndex = mIndexMap.get(adapterPosition).getGroupIndex();
        ExpandGroupItemEntity entity = mDataList.get(groupIndex);
        entity.setExpand(!entity.isExpand());
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM_TIME) {
            TitleItemHolder holder = new TitleItemHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expand_order_title, parent, false));

            return holder;
        } else {
            return new SubItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expand_order_sub, parent, false));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreatePinnedViewHolder(ViewGroup parent, int viewType) {
        TitleItemHolder holder = (TitleItemHolder) super.onCreatePinnedViewHolder(parent, viewType);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM_TIME) {
            int groupIndex = mIndexMap.get(position).getGroupIndex();
            TitleItemHolder itemHolder = (TitleItemHolder) holder;


            ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> item = mDataList.get(groupIndex);
            itemHolder.itemView.setTag(item);
            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onGroupClickListener != null) {
                        onGroupClickListener.onGroupItemClickListener(position,v);
                    }
                }
            });

            Glide.with(fragment)
                    .load(new File(item.getParent().icon))
                    .into(itemHolder.mImageExpandFlag);

        } else {
            SubItemHolder subHolder = (SubItemHolder) holder;
            int groupIndex = mIndexMap.get(position).getGroupIndex();
            int childIndex = mIndexMap.get(position).getChildIndex();
            SaleSubItemBean subItem = mDataList.get(groupIndex).getChildList().get(childIndex);
            subHolder.itemView.setTag(subItem);
            Glide.with(fragment)
                    .load(new File(subItem.icon1))
                    .into(subHolder.mImageState);
        }
    }

    @Override
    public void onBindPinnedViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindPinnedViewHolder(holder, position);
        TitleItemHolder itemHolder = (TitleItemHolder) holder;
        ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> tag = (ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>) itemHolder.itemView.getTag();
        String iconPath = tag.getParent().icon;
        Glide.with(fragment)
                .load(iconPath)
                .into(itemHolder.mImageExpandFlag);

    }


    @Override
    public boolean isPinnedPosition(int position) {
        boolean pinnedPosition = super.isPinnedPosition(position);
        System.out.println(pinnedPosition + "::pinned" + position);
        return pinnedPosition;
    }

    static class TitleItemHolder extends RecyclerView.ViewHolder {

        ImageView mImageExpandFlag;

        TitleItemHolder(View itemView) {
            super(itemView);
//            mImageExpandFlag = itemView.findViewById(com.tuacy.recyclerexpand.R.id.image_expand_flag);
        }

    }

    static class SubItemHolder extends RecyclerView.ViewHolder {

        ImageView mImageState;

        SubItemHolder(View itemView) {
            super(itemView);
//            mImageState = itemView.findViewById(com.tuacy.recyclerexpand.R.id.image_state);
        }
    }
}
