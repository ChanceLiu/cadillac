package com.mobile.cadillacsaler.adapter;

import android.support.v4.view.*;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.sale.SaleItemBean;
import com.mobile.cadillacsaler.entity.sale.SaleItemData;
import com.mobile.cadillacsaler.entity.sale.SaleSubItemBean;
import com.mobile.cadillacsaler.entity.sale.SaleSubItemData;
import com.mobile.cadillacsaler.ui.fragment.sale.SaleDetail;
import com.tuacy.recyclerexpand.expand.ExpandGroupItemEntity;

import java.io.File;
import java.util.List;

import rainbow.library.GalleryViewPager;

/**
 * Created by chanc on 2018/6/10.
 */

public class ExpandableListViewAdapter extends BaseExpandableListAdapter {
    List<ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>> data;
    MySupportFragment fragment;
    private final String carConfig = "车款配置";
    private int currentConfig = 0;
    private List<CarType.Config> cartConfigs;

    public ExpandableListViewAdapter(List<ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>> data, MySupportFragment fragment) {
        this.data = data;
        this.fragment = fragment;
    }

    public void setData(List<ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> group = data.get(groupPosition);
        List<SaleSubItemBean> childList = group.getChildList();
        if (childList == null)
            return 0;

        int size = childList.size();
        if (carConfig.equals(group.getParent().title)) {
            size += 1;//控制车型
        }

        return size;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> group = data.get(groupPosition);
        List<SaleSubItemBean> childList = group.getChildList();

        if (group.getParent().title.equals(carConfig)) {
            if (childPosition == 0) {
                if (cartConfigs==null) {
                    cartConfigs = CarType.getCartConfig(CarType.currentType);
                }
                return cartConfigs;
            } else {
                return childList.get(childPosition - 1);
            }
        }
        return childList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TitleItemHolder itemHolder = null;
        if (convertView == null) {
            itemHolder = new TitleItemHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expand_order_title, parent, false));
            convertView = itemHolder.itemView;
            convertView.setTag(itemHolder);
        } else {
            itemHolder = (TitleItemHolder) convertView.getTag();
        }

        ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> item = (ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>) getGroup(groupPosition);

        Glide.with(fragment)
                .load(new File(item.getParent().icon))
                .into(itemHolder.mImageExpandFlag);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> group = (ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean>) getGroup(groupPosition);
        if (group.getParent().title.equals(carConfig) && childPosition == 0) {
            final List<CarType.Config> child = (List<CarType.Config>) getChild(groupPosition, childPosition);

            final GalleryViewPager viewPager = (GalleryViewPager) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent, false);
            viewPager.setAdapter(new android.support.v4.view.PagerAdapter() {
                @Override
                public int getCount() {
                    return child.size();
                }

                @Override
                public boolean isViewFromObject(View view, Object object) {
                    return view == object;
                }

                @Override
                public Object instantiateItem(ViewGroup container, int position) {
                    View view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_expand_order_sub, container, false);
                    CarType.Config item = child.get(position);
                    if (currentConfig == position) {
                        item.isSelected = true;
                    }
                    Glide.with(container.getContext())
                            .load(new File(item.isSelected ? item.icon2 : item.icon1))
                            .into((ImageView) view.findViewById(R.id.image));
                    container.addView(view);
                    return view;
                }

                @Override
                public void destroyItem(ViewGroup container, int position, Object object) {
                    container.removeView((View) object);
                }

                @Override
                public float getPageWidth(int position) {
                    return 0.6f;//建议值为0.6~1.0之间
                }
            });
            viewPager.setNarrowFactor(0.6f);
            viewPager.addOnPageChangeListener(new GalleryViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (currentConfig == position) {
                        return;
                    }
                    CarType.Config config = child.get(position);

                    for (CarType.Config c : child) {
                        c.isSelected = false;
                    }
                    config.isSelected = true;

                    List<SaleSubItemBean> lightSpotItem = SaleSubItemData.getSpecLightSpotItem(CarType.currentType, config);
                    for (ExpandGroupItemEntity<SaleItemBean, SaleSubItemBean> d : data) {
                        if (carConfig.equals(d.getParent().title)) {
                            List<SaleSubItemBean> list = d.getChildList();
                            list.clear();
                            list.addAll(lightSpotItem);
                            notifyDataSetChanged();
                            currentConfig = position;

                        }
                    }
                }


                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            viewPager.setCurrentItem(currentConfig);

            return viewPager;
        }
        SubItemHolder subHolder = new SubItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expand_order_sub, parent, false));
        final SaleSubItemBean subItem = (SaleSubItemBean) getChild(groupPosition, childPosition);
        Glide.with(fragment)
                .load(new File(subItem.isSelected ? subItem.icon2 : subItem.icon1))
                .into(subHolder.mImageState);
        subHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subItem.isSelected = true;
                notifyDataSetChanged();
                if (!TextUtils.isEmpty(subItem.image)) {
                    fragment.start(SaleDetail.newInstance(subItem));
                } else {
                    Toast.makeText(fragment._mActivity, "暂时没有大图", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return subHolder.itemView;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {


        return false;
    }


    static class TitleItemHolder extends RecyclerView.ViewHolder {

        ImageView mImageExpandFlag;

        TitleItemHolder(View itemView) {
            super(itemView);
            mImageExpandFlag = itemView.findViewById(com.tuacy.recyclerexpand.R.id.image);
        }

    }

    static class SubItemHolder extends RecyclerView.ViewHolder {

        ImageView mImageState;

        SubItemHolder(View itemView) {
            super(itemView);
            mImageState = itemView.findViewById(com.tuacy.recyclerexpand.R.id.image);
        }
    }
}
