package com.mobile.cadillacsaler.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.entity.CarType;

import java.io.File;
import java.util.List;

/**
 * Created by chanc on 2018/6/10.
 */

class GalleryAdapter extends BaseAdapter {
    List<CarType.Config> configs;

    public GalleryAdapter(List<CarType.Config> configs) {
        this.configs = configs;
    }

    @Override
    public int getCount() {
        return configs.size();
    }

    @Override
    public Object getItem(int position) {
        return configs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expand_order_sub, parent, false);
        }
        CarType.Config item = (CarType.Config) getItem(position);

        Glide.with(parent.getContext())
                .load(new File(item.isSelected ? item.icon2 : item.icon1))
                .into((ImageView) convertView.findViewById(R.id.image));
        return convertView;
    }
}
