package com.mobile.cadillacsaler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.net.BeiWangLuBean;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.net.KeHuXinXiBean;
import com.mobile.cadillacsaler.ui.fragment.user.KeHuXinXi;
import com.mobile.cadillacsaler.widget.MyTextView;
import com.vise.utils.assist.DateUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 主页HomeFragment  Adapter
 */
public class KeHuXinXiAdapter extends RecyclerView.Adapter<KeHuXinXiAdapter.MyViewHolder> {
    private List<KeHuXinXiBean> mItems = new ArrayList<>();
    private LayoutInflater mInflater;
    MySupportFragment fragment;
    Context context;

    public KeHuXinXiAdapter(MySupportFragment fragment, List<KeHuXinXiBean> data) {
        this(fragment._mActivity);
        this.fragment = fragment;
        mItems = data;

    }

    public KeHuXinXiAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setDatas(List<KeHuXinXiBean> items) {
        mItems.clear();
        mItems.addAll(items);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_kehuxinxi_list, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        KeHuXinXiBean item = getItem(position);
        holder.bindData(item, position);

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public KeHuXinXiBean getItem(int position) {
        return mItems.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tvTitle)
        MyTextView mTvTitle;
        @BindView(R.id.tvDate)
        MyTextView mTvDate;
        @BindView(R.id.tvContent)
        MyTextView mTvContent;
        @BindView(R.id.tv4)
        MyTextView mTv4;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindData(final KeHuXinXiBean item, final int position) {
            SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_FORMAT2);
            String date = sdf.format(new Date(Long.parseLong(item.creattime)*1000));

            mTvTitle.setText(item.c_name+" "+item.c_tel+" "+date);
//            mTvDate.setText(date);
            mTvContent.setText(item.c_carname);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ((KeHuXinXi) fragment).delCust(item);
                    return true;
                }
            });


        }
    }

}
