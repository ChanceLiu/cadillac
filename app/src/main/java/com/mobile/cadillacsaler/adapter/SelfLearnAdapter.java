package com.mobile.cadillacsaler.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.util.PathUtil;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.entity.practice.SelfLearnBean;
import com.mobile.cadillacsaler.ui.fragment.practice.SelfLearn;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * 主页HomeFragment  Adapter
 *
 */
public class SelfLearnAdapter extends RecyclerView.Adapter<SelfLearnAdapter.MyViewHolder> {
    private List<SelfLearnBean> mItems = new ArrayList<>();
    private LayoutInflater mInflater;

    private OnItemClickListener mClickListener;
    private MySupportFragment fragment;

    public SelfLearnAdapter(MySupportFragment fragment) {
        this.fragment = fragment;
        this.mInflater = LayoutInflater.from(fragment._mActivity);
    }

    public void setDatas(List<SelfLearnBean>... items) {
        mItems.clear();
        for (List<SelfLearnBean> item : items) {
            mItems.addAll(item);
        }
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.page_image_item, parent, false);


        final MyViewHolder holder = new MyViewHolder(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (mClickListener != null) {
                    mClickListener.onItemClick(position, mItems.get(position), v);
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SelfLearnBean item = mItems.get(position);


        String file = PathUtil.sdcardPath+item.icon;

        Glide.with(fragment)
                .load(file)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(holder.imageView);

//        Observable.fromArray(fileName)
//                .map(new Function<String, byte[]>() {
//                    @Override
//                    public byte[] apply(String fileName) throws Exception {
//                        byte[] bytes = PathUtil.input2byte(fragment._mActivity.getAssets().open(fileName));
//                        return bytes;
//                    }
//                })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<byte[]>() {
//                    @Override
//                    public void accept(byte[] bytes) throws Exception {
//                        Glide.with(fragment)
//                                .load(bytes)
//                                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                                .into(holder.imageView);
//                    }
//                });


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public SelfLearnBean getItem(int position) {
        return mItems.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv);
        }
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, SelfLearnBean data, View view);
    }
}
