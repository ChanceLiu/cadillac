package com.mobile.cadillacsaler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.net.PromotionBean;
import com.mobile.cadillacsaler.net.SelfArrangeBean;
import com.mobile.cadillacsaler.ui.fragment.sale.CommonWebView;
import com.mobile.cadillacsaler.ui.fragment.sale.SelfArrange;
import com.mobile.cadillacsaler.ui.fragment.sale.SelfArrangeItemEdit;
import com.mobile.cadillacsaler.widget.MyTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 主页HomeFragment  Adapter
 */
public class SelfArrangeAdapter extends RecyclerView.Adapter<SelfArrangeAdapter.MyViewHolder> {
    private List<SelfArrangeBean> mItems = new ArrayList<>();
    private LayoutInflater mInflater;
    SelfArrange fragment;
    Context context;
    public SelfArrangeAdapter(SelfArrange fragment, List<SelfArrangeBean> data) {
        this(fragment._mActivity);
        this.fragment = fragment;
        mItems = data;

    }

    public SelfArrangeAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setDatas(List<SelfArrangeBean> items) {
        mItems.clear();
        mItems.addAll(items);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_self_arrange_list, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        SelfArrangeBean item = getItem(position);
        holder.bindData(item, position);

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public SelfArrangeBean getItem(int position) {
        return mItems.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv1)
        MyTextView mTv1;
        @BindView(R.id.imageView)
        ImageView mImageView;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindData(final SelfArrangeBean item, final int position) {

            if ("1".equals(item.valid)) {
                mImageView.setImageResource(R.mipmap.self_arrange_check_1);
            } else {
                mImageView.setImageResource(R.mipmap.self_arrange_check_0);
            }

            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ("1".equals(item.valid)) {
                        item.valid = "0";
                    } else {
                        item.valid = "1";
                    }
                    notifyDataSetChanged();
                }
            });

            mTv1.setText(item.name);

            mTv1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: 2018/6/13 编辑界面
                    fragment.start(SelfArrangeItemEdit.newInstance(item));
                }
            });
            mTv1.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    fragment.delCust(item);
                    return true;
                }
            });
        }
    }

}
