package com.mobile.cadillacsaler.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.entity.practice.TrainVideoBean;
import com.mobile.cadillacsaler.listener.OnItemClickListener;
import com.mobile.cadillacsaler.util.PathUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 主页HomeFragment  Adapter
 *
 */
public class TrainVideoAdapter extends RecyclerView.Adapter<TrainVideoAdapter.MyViewHolder> {
    private List<TrainVideoBean> mItems = new ArrayList<>();
    private LayoutInflater mInflater;
    private MySupportFragment fragment;

    private OnItemClickListener mClickListener;

    public TrainVideoAdapter(MySupportFragment fragment) {
        this.mInflater = LayoutInflater.from(fragment._mActivity);
        this.fragment=fragment;
    }

    public void setDatas(List<TrainVideoBean> items) {
        mItems.clear();
        mItems.addAll(items);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.trainvideo_item, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (mClickListener != null) {
                    mClickListener.onItemClick(position, v);
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        TrainVideoBean item = mItems.get(position);
        holder.tvTitle.setText(item.title);

        String file = PathUtil.sdcardPath+item.icon;

        Glide.with(fragment)
                .load(new File(file))
                .error(R.mipmap.cadi_icon)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public TrainVideoBean getItem(int position) {
        return mItems.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            imageView = itemView.findViewById(R.id.iv);
        }
}

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}
