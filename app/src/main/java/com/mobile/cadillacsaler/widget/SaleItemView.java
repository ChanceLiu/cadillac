package com.mobile.cadillacsaler.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.entity.sale.SaleSubItemBean;
import com.mobile.cadillacsaler.entity.sale.SaleSubItemData;
import com.mobile.cadillacsaler.ui.fragment.sale.SaleDetail;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chanc on 2018/6/9.
 */

public class SaleItemView extends LinearLayout {

    @BindView(R.id.item_img)
    ImageView mItemImg;
    @BindView(R.id.sub_item_container)
    LinearLayout mSubItemContainer;
    MySupportFragment fragment;

    public SaleItemView(Context context) {
        super(context);
        init();
    }

    public void setFragment(MySupportFragment fragment) {
        this.fragment = fragment;
    }

    public SaleItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SaleItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.sale_item_detail, this, true);
        ButterKnife.bind(this);
    }


    public void setItemImg(int res) {
        mItemImg.setImageResource(res);
    }


    public boolean isSub0Show = false;

    /**
     * 点亮车型
     *
     * @param isShow
     */
    public void togle(boolean isShow) {
        isSub0Show = isShow;
        mSubItemContainer.setVisibility(isShow ? VISIBLE : GONE);
        if (isSub0Show) {
            List<View> item0Views = getItem0Views();
            for (View item0View : item0Views) {
                mSubItemContainer.addView(item0View);
            }
        }
    }

    public List<View> getItem0Views() {
        List<SaleSubItemBean> items = SaleSubItemData.getHotspotItem(CarType.currentType);
        List<View> views = new ArrayList<>();
        for (SaleSubItemBean item : items) {
            View subItem = LayoutInflater.from(getContext()).inflate(R.layout.sale_item_sub_0, mSubItemContainer, false);
            Glide.with(getContext())
                    .load(item.icon1)
                    .into(((ImageView) subItem.findViewById(R.id.item_img)));
            subItem.setTag(item);
            subItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    SaleSubItemBean bean = (SaleSubItemBean) v.getTag();
                    Glide.with(getContext())
                            .load(bean.icon2)
                            .into(((ImageView) v.findViewById(R.id.item_img)));

                    fragment.start(SaleDetail.newInstance(bean));

                }
            });
            views.add(subItem);

        }

        return views;
    }

    public void togle() {
        togle(!isSub0Show);
    }


}
