package com.mobile.cadillacsaler.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.davemorrissey.labs.subscaleview.decoder.DecoderFactory;
import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.util.ScreenUtils;
import com.vise.utils.file.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by WangPing on 2018/1/12.
 */

public class LongPicView extends FrameLayout {
    private RecyclerView picList;
    private Bitmap[] picArr;
    private int contentHeight;
    private int contentWidth;


    public LongPicView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public LongPicView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LongPicView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
//        int screenHeight = ScreenUtils.getScreenHeight(context);
//        int statusHeight = ScreenUtils.getStatusHeight(context);
//        int naviHeight = 0;
//        contentHeight = screenHeight - statusHeight - naviHeight;

        picList = new RecyclerView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        picList.setLayoutParams(params);


        picList.setLayoutManager(new LinearLayoutManager(context));
        this.addView(picList);
    }



    public void setImageSource(final String  file) {
        post(new Runnable() {
            @Override
            public void run() {
                contentHeight = getHeight();
                contentWidth = getWidth();
                if (contentHeight == 0) {
                    //没有显示
                    return;
                }
                BitmapFactory.Options tmpOptions = new BitmapFactory.Options();
                tmpOptions.inJustDecodeBounds = true;
                try {
                    BitmapFactory.decodeStream(new FileInputStream(new File(file)), null, tmpOptions);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }


                //图片宽高
                int imageWidth = tmpOptions.outWidth;
                int imageHeight = tmpOptions.outHeight;

//                BitmapFactory.Options newOpts = new BitmapFactory.Options();
//                newOpts.inJustDecodeBounds = false;
//                int w = newOpts.outWidth;
//                int h = newOpts.outHeight;
                // 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
//                float hh = contentHeight;// 这里设置高度为800f
//                float ww = contentWidth;// 这里设置宽度为480f
                // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
//                int be = 1;// be=1表示不缩放

//                 be = (int) (imageWidth / contentWidth);

//                if (be <= 0)
//                    be = 1;
//                newOpts.inSampleSize = be;// 设置缩放比例
//                // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
//                BitmapFactory.decodeStream(is, null, newOpts);
//
                int clipH=  imageWidth/contentWidth *contentHeight;
                if (clipH == 0) {
                    System.out.println("控件,高度为空?");
                    return;
                }
                int picListSize;
                if (imageHeight % contentHeight == 0) {
                    picListSize = imageHeight / clipH;
                } else {
                    picListSize = imageHeight / clipH + 1;
                }
                picArr = new Bitmap[picListSize];
                try {
                    BitmapRegionDecoder bitmapRegionDecoder = BitmapRegionDecoder.newInstance(file, false);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.RGB_565;


                    for (int i = 0; i < picListSize; i++) {
                        Rect rect = new Rect();
                        rect.left = 0;
                        rect.top = (int) (clipH * i);
                        rect.right = imageWidth;
                        int clipHeight = (int) (clipH * (i + 1));
                        if (clipHeight >= imageHeight) {
                            rect.bottom = imageHeight;
                        } else {
                            rect.bottom = clipHeight;
                        }
                        Bitmap bitmap = bitmapRegionDecoder.decodeRegion(rect, options);
                        picArr[i] = bitmap;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                PicAdapter adapter = new PicAdapter(contentHeight,contentWidth);
                picList.setAdapter(adapter);
            }
        });
    }


    private class PicAdapter extends RecyclerView.Adapter<ViewHolder> {
        int height;
        int width;

        public PicAdapter(int height, int width) {
            this.height = height;
            this.width = width;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final ImageView image = (ImageView) LayoutInflater.from(parent.getContext()).inflate(R.layout.long_page_item, parent, false);
//            final ImageView image = new ImageView(parent.getContext());

//            image.setScaleType(ImageView.ScaleType.FIT_XY);

            return new ViewHolder(image);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Bitmap bm = picArr[position];
            //按照bitmap的宽高,动态计算ImageView的宽高,进行适配
            int h = bm.getHeight();
            int w = bm.getWidth();
            //width / w =height /h
            ImageView image = (ImageView) holder.itemView;

            ViewGroup.LayoutParams p = image.getLayoutParams();
            p.width = width;
            p.height = (int) ((float)width/w * h);
            image.setLayoutParams(p);

            image.setScaleType(ImageView.ScaleType.FIT_XY);
            image.setImageBitmap(bm);

        }

        @Override
        public int getItemCount() {
            return picArr.length;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }
}