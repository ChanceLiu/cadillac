package com.mobile.cadillacsaler.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by chanc on 2018/6/5.
 */

public class MyTextView extends android.support.v7.widget.AppCompatTextView {
    public MyTextView(Context context) {
        super(context);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        //得到AssetManager
        AssetManager mgr=context.getAssets();
        //根据路径得到 Typeface
       Typeface tf=Typeface.createFromAsset(mgr, "fonts/hyjh50j.ttf");

        //设置字体
        setTypeface(tf);


    }
}
