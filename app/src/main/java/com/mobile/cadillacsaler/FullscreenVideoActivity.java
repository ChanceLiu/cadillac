package com.mobile.cadillacsaler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.VideoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenVideoActivity extends AppCompatActivity {

    @BindView(R.id.videoplayer)
    VideoView mVideoplayer;
    @BindView(R.id.back)
    ImageButton mBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_fullscreen_video);
        ButterKnife.bind(this);
        String path = getIntent().getStringExtra("path");

        mVideoplayer.setVideoPath(path);

        mVideoplayer.start();
    }

    @OnClick(R.id.back)
    public void onClick() {
        mVideoplayer.stopPlayback();
        finish();
    }
}
