package com.mobile.cadillacsaler.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by chanc on 2018/6/5.
 */

public class PathUtil {
    public static final String sdcardPath = "/sdcard/CadillacSaler/";
//    public static final String sdcardPath = "file:///android_asset/";



    // 将输入流解析成字节数组
    public static final byte[] input2byte(InputStream inStream)
            throws IOException {
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc = 0;
        while ((rc = inStream.read(buff, 0, 100)) > 0) {
            swapStream.write(buff, 0, rc);
        }
        byte[] in2b = swapStream.toByteArray();
        return in2b;
    }
}
