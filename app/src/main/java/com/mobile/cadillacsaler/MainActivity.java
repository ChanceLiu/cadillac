package com.mobile.cadillacsaler;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobile.cadillacsaler.base.BaseMainFragment;
import com.mobile.cadillacsaler.base.MySupportActivity;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.net.Const;
import com.mobile.cadillacsaler.ui.fragment.user.UserInfo;
import com.mobile.cadillacsaler.ui.fragment.start.Launch;
import com.mobile.cadillacsaler.ui.fragment.SaleLogin;
import com.mobile.cadillacsaler.util.PathUtil;
import com.vise.log.ViseLog;
import com.vise.xsnow.common.ViseConfig;
import com.vise.xsnow.widget.GlideCircleTransform;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.yokeyword.fragmentation.ISupportFragment;
import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * 流程式demo  tip: 多使用右上角的"查看栈视图"
 * Created by YoKeyword on 16/1/29.
 */
public class MainActivity extends MySupportActivity
        implements SaleLogin.OnLoginSuccessListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    // 再点一次退出程序时间设置
    private static final long WAIT_TIME = 2000L;
    @BindView(R.id.fl_container)
    FrameLayout flContainer;
    @BindView(R.id.img_account)
    ImageView imgAccount;


    private long TOUCH_TIME = 0;
    private File file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        MySupportFragment fragment = findFragment(Launch.class);
        if (fragment == null) {
            loadRootFragment(R.id.fl_container, Launch.newInstance());
        }
        initView();

        setFragmentAnimator(new DefaultHorizontalAnimator());
        requestAllPower();
    }

    public void showImgAccount(boolean isShow) {
        imgAccount.setVisibility(isShow ? View.VISIBLE : View.GONE);


    }

    /**
     * 设置动画，也可以使用setFragmentAnimator()设置
     */
    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        // 设置默认Fragment动画  默认竖向(和安卓5.0以上的动画相同)
        return super.onCreateFragmentAnimator();
        // 设置横向(和安卓4.x动画相同)
//        return new DefaultHorizontalAnimator();
        // 设置自定义动画
//        return new FragmentAnimator(enter,exit,popEnter,popExit);
    }

    private void initView() {
        Glide.with(this)
                .load(R.mipmap.head_icon)
                .transform(new GlideCircleTransform(this))
                .into(imgAccount);

    }

    @Override
    public void onBackPressedSupport() {
        ISupportFragment topFragment = getTopFragment();

        // 主页的Fragment
        if (topFragment instanceof BaseMainFragment) {

        }

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            pop();
        } else {
            exit();
        }
    }

    public void exit() {
        if (System.currentTimeMillis() - TOUCH_TIME < WAIT_TIME) {
            finish();
        } else {
            TOUCH_TIME = System.currentTimeMillis();
            Toast.makeText(this, R.string.press_again_exit, Toast.LENGTH_SHORT).show();
        }
    }


    private void goLogin() {
        start(SaleLogin.newInstance());
    }

    @Override
    public void onLoginSuccess(String account) {
        Toast.makeText(this, R.string.sign_in_success, Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.img_account)
    public void onClick() {
        start(UserInfo.newInstance());
    }


    public static final int REQUEST_CAMERA = 101;
    public static final int REQUEST_PERMISSION = 100;
    public static final int REQUEST_PERMISSION_CAMERA = 110;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PERMISSION_GRANTED) {
                    Toast.makeText(this, "" + "权限" + permissions[i] + "申请成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "" + "权限" + permissions[i] + "申请失败", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == REQUEST_PERMISSION_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PERMISSION_GRANTED) {
                    Toast.makeText(this, "" + "权限" + permissions[i] + "申请成功", Toast.LENGTH_SHORT).show();
                    useCamera();
                } else {
                    Toast.makeText(this, "" + "权限" + permissions[i] + "申请失败", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    /**
     * 权限动态获取
     */
    public void requestAllPower() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION);
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {

            }
        }
    }
    /**
     * 权限动态获取
     */

    /**
     * 使用相机
     */
    public void useCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
//
//            } else {
//
//            }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_PERMISSION);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
//                    + "/CadillacSaler/bg.jpg");
            file = new File( Const.BG_PATH);
            file.getParentFile().mkdirs();

            //改变Uri  com.mobile.cadillacsaler.fileprovider 注意和xml中的一致
            Uri uri = FileProvider.getUriForFile(this, "com.mobile.cadillacsaler.fileprovider", file);
            //添加权限
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

//            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            ViseLog.d(file);
            //在手机相册中显示刚拍摄的图片
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(file);
            mediaScanIntent.setData(contentUri);
            sendBroadcast(mediaScanIntent);
        }
    }
}
