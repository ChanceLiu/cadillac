package com.mobile.cadillacsaler.entity.practice;

import java.util.ArrayList;

/**
 * Created by chanc on 2018/6/5.
 */

public class SelfLearnData {
    int carType;
    public ArrayList<String> mTitles = new ArrayList<>();
    public ArrayList<SelfLearnBean> mItems1 = new ArrayList<>();
    public ArrayList<SelfLearnBean> mItems2 = new ArrayList<>();
    public ArrayList<SelfLearnBean> mItems3 = new ArrayList<>();

    public SelfLearnData(int carType) {
        this.carType = carType;
        initData(carType);
    }

    private void initData(int carType) {
        switch (carType) {
            case 0:
                mTitles.add("全部");
                mTitles.add("豪华互联");
                mTitles.add("运动美学");
                mTitles.add("卓越性能");

                mItems1.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_0.png", "SelfLearn/0/image/self_learn_image_0_0.jpg","", false));
                mItems1.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_1.png", "SelfLearn/0/image/self_learn_image_0_1.jpg","SelfLearn/0/video/self_learn_video_0_1.mp4", false));
                mItems1.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_2.png", "SelfLearn/0/image/self_learn_image_0_2.jpg","", false));
                mItems2.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_3.png", "SelfLearn/0/image/self_learn_image_0_3.jpg","", true));
                mItems2.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_4.png", "SelfLearn/0/image/self_learn_image_0_4.jpg","", true));
                mItems2.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_5.png", "SelfLearn/0/image/self_learn_image_0_5.jpg","", false));
                mItems3.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_6.png", "SelfLearn/0/image/self_learn_image_0_6.jpg","", true));
                mItems3.add(new SelfLearnBean("SelfLearn/0/icon/self_learn_icon_0_7.png", "SelfLearn/0/image/self_learn_image_0_7.jpg","", true));


                break;
            case 1:
                mTitles.add("全部");
                mTitles.add("创新格调");
                mTitles.add("人性科技");
                mTitles.add("周全防护");

                mItems1.add(new SelfLearnBean("SelfLearn/1/icon/self_learn_icon_1_0.png", "SelfLearn/1/image/self_learn_image_1_0.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/1/icon/self_learn_icon_1_1.png", "SelfLearn/1/image/self_learn_image_1_1.jpg","", true));
                mItems2.add(new SelfLearnBean("SelfLearn/1/icon/self_learn_icon_1_2.png", "SelfLearn/1/image/self_learn_image_1_2.jpg","SelfLearn/1/video/self_learn_video_1_2.mp4", true));
                mItems2.add(new SelfLearnBean("SelfLearn/1/icon/self_learn_icon_1_3.png", "SelfLearn/1/image/self_learn_image_1_3.jpg","SelfLearn/1/video/self_learn_video_1_3.mp4", false));
                mItems3.add(new SelfLearnBean("SelfLearn/1/icon/self_learn_icon_1_4.png", "SelfLearn/1/image/self_learn_image_1_4.jpg","SelfLearn/1/video/self_learn_video_1_4.mp4", true));
                mItems3.add(new SelfLearnBean("SelfLearn/1/icon/self_learn_icon_1_5.png", "SelfLearn/1/image/self_learn_image_1_5.jpg","", true));


                break;
            case 2:
                mTitles.add("全部");
                mTitles.add("美式风范");
                mTitles.add("顶级材质");
                mTitles.add("黑科技");

                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_0.png", "SelfLearn/2/image/self_learn_image_2_0.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_1.png", "SelfLearn/2/image/self_learn_image_2_1.jpg","", false));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_2.png", "SelfLearn/2/image/self_learn_image_2_2.jpg","", false));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_3.png", "SelfLearn/2/image/self_learn_image_2_3.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_4.png", "SelfLearn/2/image/self_learn_image_2_4.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_5.png", "SelfLearn/2/image/self_learn_image_2_5.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_6.png", "SelfLearn/2/image/self_learn_image_2_6.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_7.png", "SelfLearn/2/image/self_learn_image_2_7.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_8.png", "SelfLearn/2/image/self_learn_image_2_8.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_9.png", "SelfLearn/2/image/self_learn_image_2_9.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_10.png", "SelfLearn/2/image/self_learn_image_2_10.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_11.png", "SelfLearn/2/image/self_learn_image_2_11.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/2/icon/self_learn_icon_2_12.png", "SelfLearn/2/image/self_learn_image_2_12.jpg","", false));


                break;
            case 3:
                mTitles.add("全部");
                mTitles.add("创新科技");
                mTitles.add("风尚设计");
                mTitles.add("无忧安全");
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_0.png", "SelfLearn/3/image/self_learn_image_3_0.jpg","SelfLearn/3/video/self_learn_video_3_0.mp4", true));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_1.png", "SelfLearn/3/image/self_learn_image_3_1.jpg","", true));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_2.png", "SelfLearn/3/image/self_learn_image_3_2.jpg","SelfLearn/3/video/self_learn_video_3_2.mp4", true));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_3.png", "SelfLearn/3/image/self_learn_image_3_3.jpg","SelfLearn/3/video/self_learn_video_3_3.mp4", false));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_4.png", "SelfLearn/3/image/self_learn_image_3_4.jpg","", false));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_5.png", "SelfLearn/3/image/self_learn_image_3_5.jpg","SelfLearn/3/video/self_learn_video_3_5.mp4", true));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_6.png", "SelfLearn/3/image/self_learn_image_3_6.jpg","", false));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_7.png", "SelfLearn/3/image/self_learn_image_3_7.jpg","SelfLearn/3/video/self_learn_video_3_7.mp4", true));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_8.png", "SelfLearn/3/image/self_learn_image_3_8.jpg","SelfLearn/3/video/self_learn_video_3_8.mp4", false));
                mItems1.add(new SelfLearnBean("SelfLearn/3/icon/self_learn_icon_3_9.png", "SelfLearn/3/image/self_learn_image_3_9.jpg","", false));

                break;
        }
    }
}
