package com.mobile.cadillacsaler.entity.brandManager;

import com.mobile.cadillacsaler.entity.BaseBean;

/**
 * Created by chanc on 2018/6/7.
 */

public class BrandImageBean extends BaseBean {
    public boolean isVideo=false;
    public String title;
    public String desc;

    public BrandImageBean( String image, String video, String title, String desc) {
        super("", image, video);
        this.title = title;
        this.desc = desc;
    }
}
