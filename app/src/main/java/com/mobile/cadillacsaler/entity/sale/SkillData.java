package com.mobile.cadillacsaler.entity.sale;

import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.util.PathUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chanc on 2018/6/8.
 */

public class SkillData {

    public static SkillDataBean getSkillData(int carType) {
        SkillDataBean sd = new SkillDataBean();
        ArrayList<String> keys = new ArrayList<>();
        Map<String,String> iconAndContent=new HashMap<>();
        sd.keys = keys;
        sd.iconsAndContents = iconAndContent;
        switch (carType) {
            case CarType.ats_l:  sd.bigImage = PathUtil.sdcardPath+"Skill/0/unique_skill_0.png";
                iconAndContent.put(PathUtil.sdcardPath + "Skill/0/unique_skill_0_icon_0.png", PathUtil.sdcardPath + "Skill/0/unique_skill_0_image_0.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/0/unique_skill_0_icon_1.png", PathUtil.sdcardPath + "Skill/0/unique_skill_0_image_1.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/0/unique_skill_0_icon_2.png", PathUtil.sdcardPath + "Skill/0/unique_skill_0_image_2.png");
                keys.add(PathUtil.sdcardPath + "Skill/0/unique_skill_0_icon_0.png");
                keys.add(PathUtil.sdcardPath + "Skill/0/unique_skill_0_icon_1.png");
                keys.add(PathUtil.sdcardPath + "Skill/0/unique_skill_0_icon_2.png");

            break;
            case CarType.xts:    sd.bigImage = PathUtil.sdcardPath+"Skill/1/unique_skill_1.png";
                iconAndContent.put(PathUtil.sdcardPath + "Skill/1/unique_skill_1_icon_0.png", PathUtil.sdcardPath + "Skill/1/unique_skill_1_image_0.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/1/unique_skill_1_icon_1.png", PathUtil.sdcardPath + "Skill/1/unique_skill_1_image_1.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/1/unique_skill_1_icon_2.png", PathUtil.sdcardPath + "Skill/1/unique_skill_1_image_2.png");

                keys.add(PathUtil.sdcardPath + "Skill/1/unique_skill_1_icon_0.png");
                keys.add(PathUtil.sdcardPath + "Skill/1/unique_skill_1_icon_1.png");
                keys.add(PathUtil.sdcardPath + "Skill/1/unique_skill_1_icon_2.png");
                break;
            case CarType.ct6:    sd.bigImage = PathUtil.sdcardPath+"Skill/2/unique_skill_2.png";
                iconAndContent.put(PathUtil.sdcardPath + "Skill/2/unique_skill_2_icon_0.png", PathUtil.sdcardPath + "Skill/2/unique_skill_2_image_0.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/2/unique_skill_2_icon_1.png", PathUtil.sdcardPath + "Skill/2/unique_skill_2_image_1.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/2/unique_skill_2_icon_2.png", PathUtil.sdcardPath + "Skill/2/unique_skill_2_image_2.png");
                keys.add(PathUtil.sdcardPath + "Skill/2/unique_skill_2_icon_0.png");
                keys.add(PathUtil.sdcardPath + "Skill/2/unique_skill_2_icon_1.png");
                keys.add(PathUtil.sdcardPath + "Skill/2/unique_skill_2_icon_2.png");
                break;
            case CarType.xt5:    sd.bigImage = PathUtil.sdcardPath+"Skill/3/unique_skill_3.png";
                iconAndContent.put(PathUtil.sdcardPath + "Skill/3/unique_skill_3_icon_0.png", PathUtil.sdcardPath + "Skill/3/unique_skill_3_image_0.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/3/unique_skill_3_icon_1.png", PathUtil.sdcardPath + "Skill/3/unique_skill_3_image_1.png");
                iconAndContent.put(PathUtil.sdcardPath + "Skill/3/unique_skill_3_icon_2.png", PathUtil.sdcardPath + "Skill/3/unique_skill_3_image_2.png");
                keys.add(PathUtil.sdcardPath + "Skill/3/unique_skill_3_icon_0.png");
                keys.add(PathUtil.sdcardPath + "Skill/3/unique_skill_3_icon_1.png");
                keys.add(PathUtil.sdcardPath + "Skill/3/unique_skill_3_icon_2.png");
            break;

        }

        return sd;
    }

}
