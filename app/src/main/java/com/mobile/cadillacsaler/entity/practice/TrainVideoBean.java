package com.mobile.cadillacsaler.entity.practice;

import com.mobile.cadillacsaler.entity.BaseBean;

/**
 * Created by chanc on 2018/6/6.
 */

public class TrainVideoBean extends BaseBean {
    public String title;
    TrainVideoBean(String title,  String video,String icon) {
        super(icon, "", video);
        this.title = title;
    }
}
