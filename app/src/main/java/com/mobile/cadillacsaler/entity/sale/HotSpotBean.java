package com.mobile.cadillacsaler.entity.sale;

/**
 * Created by chanc on 2018/6/10.
 */

public class HotSpotBean extends SaleSubItemBean {
    public HotSpotBean(String icon1, String icon2, String image, String video) {
        super(icon1, icon2, image, video);
    }
}
