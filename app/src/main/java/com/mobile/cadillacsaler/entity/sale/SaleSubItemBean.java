package com.mobile.cadillacsaler.entity.sale;

import java.io.Serializable;

/**
 * Created by chanc on 2018/6/8.
 */

public class SaleSubItemBean implements Serializable {


    public String icon1;
    public String icon2;
    public String image;
    public String video;
    public boolean isSelected;

    public SaleSubItemBean(String icon1, String icon2, String image, String video) {
        this.icon1 = icon1;
        this.icon2 = icon2;
        this.image = image;
        this.video = video;
    }
}
