package com.mobile.cadillacsaler.entity.practice;

import com.mobile.cadillacsaler.entity.BaseBean;

/**
 * Created by chanc on 2018/6/5.
 */

public class SelfLearnBean extends BaseBean {
    public boolean toOtherPage;


    public SelfLearnBean(String icon, String image, String video, boolean toOtherPage) {
        super(icon, image, video);
        this.toOtherPage = toOtherPage;
    }
}
