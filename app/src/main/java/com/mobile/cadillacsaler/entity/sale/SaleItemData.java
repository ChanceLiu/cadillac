package com.mobile.cadillacsaler.entity.sale;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.base.MySupportFragment;
import com.mobile.cadillacsaler.ui.fragment.sale.SaleItemClickListener;
import com.mobile.cadillacsaler.util.PathUtil;
import com.mobile.cadillacsaler.widget.SaleItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chanc on 2018/6/8.
 */

public class SaleItemData {

    public static String getItemDesc(int index) {
        String i="";
        switch (index) {
            case 0:i="通过简单的话术和图片,小视频,有步骤地介绍车型亮点" ;break;
            case 1:i="每个车系下不同车款配置的主要亮点一一介绍" ;break;
            case 2:i="将车型的亮点以组合的方式讲解出本车最突出吸引人的三个优势" ;break;
            case 3:i="在带客户试乘试驾过程中,准确完善的介绍出本车必须体验的亮点." ;break;
            case 4:i="将购车会产生的费用一一罗列,快速计算出购车总费用." ;break;
            case 5:i="通过本车与竞争车型对比后,一目了然本车优势." ;break;
            case 6:i="快速查询共计十个维度的网络车友对本品的真实评价,以及评价截图" ;break;
            case 7:i="本品牌内同系车高低车款进行对比,查看价格差距,配置差距,优势总结" ;break;
        }
        return i;
    }



    public static int getPosition(String itemName) {
        int i;
        switch (itemName) {
            case "车型亮点":i=0 ;break;
            case "车款配置":i=1 ;break;
            case "三大卖点":i=2 ;break;
            case "试乘试驾":i=3 ;break;
            case "综合报价":i=4 ;break;
            case "竞品对比":i=5 ;break;
            case "网络口碑":i=6 ;break;
            case "自我对比":i=7 ;break;
            default:
                i = -1;
        }
        return i;
    }
    public static String getItemName(int index) {
        String i="";
        switch (index) {
            case 0:i="车型亮点" ;break;
            case 1:i="车款配置" ;break;
            case 2:i="三大卖点" ;break;
            case 3:i="试乘试驾" ;break;
            case 4:i="综合报价" ;break;
            case 5:i="竞品对比" ;break;
            case 6:i="网络口碑" ;break;
            case 7:i="自我对比" ;break;
        }
        return i;
    }


    public static SaleItemBean getSaleItemBean(String itemName) {
        SaleItemBean itemBean = new SaleItemBean();
        switch (itemName) {
            case "车型亮点":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_0_0.png";itemBean.title= "车型亮点";    ;break;
            case "车款配置":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_1_0.png";itemBean.title= "车款配置";     ;break;
            case "三大卖点":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_2_0.png";itemBean.title= "三大卖点";     ;break;
            case "试乘试驾":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_3_0.png";itemBean.title= "试乘试驾";     ;break;
            case "综合报价":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_4_0.png";itemBean.title= "综合报价";     ;break;
            case "竞品对比":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_5_0.png";itemBean.title= "竞品对比";     ;break;
            case "网络口碑":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_6_0.png";itemBean.title= "网络口碑";     ;break;
            case "自我对比":itemBean.icon=PathUtil.sdcardPath+"Sale/sale_tab_btn_7_0.png";itemBean.title= "自我对比";     ;break;
        }

        return itemBean;
    }




    /**
     * //0 车型亮点
     * //1 车款配置
     * //2 三大卖点
     * //3 试乘试驾
     * //4 综合报价
     * //5 竞品对比
     * //6 网络口碑
     * //7 自我对比
     *
     * @param fragment
     * @param root
     * @return
     */
    public static List<SaleItemView> getItems(MySupportFragment fragment, ViewGroup root) {
        ArrayList<SaleItemView> views = new ArrayList<>();
        SaleItemView btn0 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);
        SaleItemView btn1 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);
        SaleItemView btn2 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);
        SaleItemView btn3 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);
        SaleItemView btn4 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);
        SaleItemView btn5 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);
        SaleItemView btn6 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);
        SaleItemView btn7 = (SaleItemView) LayoutInflater.from(fragment._mActivity).inflate(R.layout.sale_item, root, false);

        btn0.setItemImg(R.mipmap.sale_tab_btn_0_0_3x);
        btn1.setItemImg(R.mipmap.sale_tab_btn_1_0_3x);
        btn2.setItemImg(R.mipmap.sale_tab_btn_2_0_3x);
        btn3.setItemImg(R.mipmap.sale_tab_btn_3_0_3x);
        btn4.setItemImg(R.mipmap.sale_tab_btn_4_0_3x);
        btn5.setItemImg(R.mipmap.sale_tab_btn_5_0_3x);
        btn6.setItemImg(R.mipmap.sale_tab_btn_6_0_3x);
        btn7.setItemImg(R.mipmap.sale_tab_btn_7_0_3x);

        btn0.setId(R.mipmap.sale_tab_btn_0_0_3x+0);
        btn1.setId(R.mipmap.sale_tab_btn_1_0_3x+0);
        btn2.setId(R.mipmap.sale_tab_btn_2_0_3x+0);
        btn3.setId(R.mipmap.sale_tab_btn_3_0_3x+0);
        btn4.setId(R.mipmap.sale_tab_btn_4_0_3x+0);
        btn5.setId(R.mipmap.sale_tab_btn_5_0_3x+0);
        btn6.setId(R.mipmap.sale_tab_btn_6_0_3x+0);
        btn7.setId(R.mipmap.sale_tab_btn_7_0_3x+0);


        views.add(btn0);
        views.add(btn1);
        views.add(btn2);
        views.add(btn3);
        views.add(btn4);
        views.add(btn5);
        views.add(btn6);
        views.add(btn7);
        for (SaleItemView item : views) {
            item.setOnClickListener(new SaleItemClickListener(fragment));
            item.setFragment(fragment);
        }

        return views;
    }


    //-------车型亮点





}
