package com.mobile.cadillacsaler.entity;

import java.io.Serializable;

/**
 *
 */
public class Content implements Serializable{
    private String title;
    private String content;
    private int imgRes;

    public Content(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Content(String title, int imgRes) {
        this.title = title;
        this.imgRes = imgRes;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    @Override
    public String toString() {
        return "Content{" +
                "title='" + title + '\'' +
                ", desc='" + content + '\'' +
                ", imgRes=" + imgRes +
                '}';
    }
}
