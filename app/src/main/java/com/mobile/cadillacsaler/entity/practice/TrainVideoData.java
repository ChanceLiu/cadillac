package com.mobile.cadillacsaler.entity.practice;

import java.util.ArrayList;

/**
 * Created by chanc on 2018/6/6.
 */










public class TrainVideoData {
    public ArrayList<TrainVideoBean> data;
    {
        data = new ArrayList<>();
//        "TrainVideo/video/train_video_video_0.mp4"
        data.add(new TrainVideoBean("销售流程之客户接待",  "TrainVideo/video/train_video_video_0.mp4",  "TrainVideo/image/train_video_icon_0.png"));
        data.add(new TrainVideoBean("销售流程之需求分析",  "TrainVideo/video/train_video_video_1.mp4",  "TrainVideo/image/train_video_icon_1.png"));
        data.add(new TrainVideoBean("销售流程之产品介绍",  "TrainVideo/video/train_video_video_2.mp4",  "TrainVideo/image/train_video_icon_2.png"));
        data.add(new TrainVideoBean("销售流程之试乘试驾",  "TrainVideo/video/train_video_video_3.mp4",  "TrainVideo/image/train_video_icon_3.png"));
        data.add(new TrainVideoBean("销售流程之促进成交",  "TrainVideo/video/train_video_video_4.mp4",  "TrainVideo/image/train_video_icon_4.png"));
        data.add(new TrainVideoBean("销售流程之购车交易",  "TrainVideo/video/train_video_video_5.mp4",  "TrainVideo/image/train_video_icon_5.png"));
        data.add(new TrainVideoBean("销售流程之金钥匙交车", "TrainVideo/video/train_video_video_6.mp4", "TrainVideo/image/train_video_icon_6.png" ));
        data.add(new TrainVideoBean("销售流程之交车后接触", "TrainVideo/video/train_video_video_7.mp4", "TrainVideo/image/train_video_icon_7.png" ));

    }
}
