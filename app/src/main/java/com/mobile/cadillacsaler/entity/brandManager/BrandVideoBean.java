package com.mobile.cadillacsaler.entity.brandManager;

import com.mobile.cadillacsaler.entity.BaseBean;

/**
 * Created by chanc on 2018/6/7.
 */

public class BrandVideoBean extends BrandImageBean {


    public BrandVideoBean( String image, String video, String title, String content) {
        super( image, video, title, content);
        isVideo = true;
    }
}
