package com.mobile.cadillacsaler.entity;

import java.io.Serializable;

/**
 * Created by chanc on 2018/6/7.
 */

public class BaseBean implements Serializable {
    public String icon;
    public String image;
    public String video;

    public BaseBean(String icon, String image, String video) {
        this.icon = icon;
        this.image = image;
        this.video = video;
    }
}
