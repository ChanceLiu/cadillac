package com.mobile.cadillacsaler.entity.sale;

import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.util.PathUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chanc on 2018/6/8.
 */

public class SaleSubItemData implements Serializable {

    public static List<SaleSubItemBean> getSaleSubItem(String saleItem) {
        switch (saleItem) {
            case "车型亮点":
                return getHotspotItem(CarType.currentType);
            case "车款配置":
                return getSpecLightSpotItem(CarType.currentType,new CarType.Config(0,"",""));

            case "三大卖点":
                ;
                break;
            case "试乘试驾":
                ;
                break;
            case "综合报价":
                ;
                break;
            case "竞品对比":
                ;
                break;
            case "网络口碑":
                ;
                break;
            case "自我对比":
                ;
                break;
        }
        return null;
    }


    public static List<SaleSubItemBean> getHotspotItem(int carType) {
        List<SaleSubItemBean> data = new ArrayList<>();

        switch (carType) {
            case CarType.ats_l:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_0_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_0_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_0.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_1_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_1_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_1.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_2_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_2_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_2.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_3_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_3_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_3.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_4_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_4_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_4.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_5_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_5_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_5.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_6_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_6_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_6.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_7_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_7_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_7.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_8_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_8_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_8.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_9_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_9_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_9.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_10_0.png", PathUtil.sdcardPath + "HotSpot/0/icon/hot_spot_icon_0_10_1.png", PathUtil.sdcardPath + "HotSpot/0/image/hot_spot_image_0_10.jpg", ""));


                break;
            case CarType.xts:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_0_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_0_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_0.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_1_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_1_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_1.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_2_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_2_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_2.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_3_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_3_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_3.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_4_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_4_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_4.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_5_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_5_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_5.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_6_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_6_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_6.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_7_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_7_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_7.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_8_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_8_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_8.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_9_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_9_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_9.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_10_0.png", PathUtil.sdcardPath + "HotSpot/1/icon/hot_spot_icon_1_10_1.png", PathUtil.sdcardPath + "HotSpot/1/image/hot_spot_image_1_10.jpg", ""));


                break;
            case CarType.ct6:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_0_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_0_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_0.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_1_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_1_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_1.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_2_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_2_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_2.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_3_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_3_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_3.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_4_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_4_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_4.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_5_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_5_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_5.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_6_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_6_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_6.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_7_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_7_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_7.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_8_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_8_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_8.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_9_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_9_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_9.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_10_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_10_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_10.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_11_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_11_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_11.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_12_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_12_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_12.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_13_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_13_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_13.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_14_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_14_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_14.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_15_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_15_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_15.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_16_0.png", PathUtil.sdcardPath + "HotSpot/2/icon/hot_spot_icon_2_16_1.png", PathUtil.sdcardPath + "HotSpot/2/image/hot_spot_image_2_16.jpg", ""));


                break;
            case CarType.xt5:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_0_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_0_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_0.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_1_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_1_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_1.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_2_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_2_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_2.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_3_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_3_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_3.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_4_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_4_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_4.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_5_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_5_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_5.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_6_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_6_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_6.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_7_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_7_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_7.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_8_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_8_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_8.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_9_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_9_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_9.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_10_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_10_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_10.jpg", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_11_0.png", PathUtil.sdcardPath + "HotSpot/3/icon/hot_spot_icon_3_11_1.png", PathUtil.sdcardPath + "HotSpot/3/image/hot_spot_image_3_11.jpg", ""));


                break;
        }

        return data;


    }


    /**
     * todo 车型配置 icon 对应大图需补全
     *
     * @param carType
     * @return
     */
    public static List<SaleSubItemBean> getLightSpotItem(int carType) {
        List<SaleSubItemBean> data = new ArrayList<>();

        switch (carType) {

            case CarType.ats_l:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_0_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_0_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_1_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_1_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_2_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_2_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_3_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_3_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_4_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_4_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_5_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_5_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_6_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_6_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_7_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_7_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_8_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_8_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_9_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_9_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_10_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_10_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_11_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_11_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_12_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_12_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_13_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_13_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_14_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_14_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_15_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_15_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_16_0.png", PathUtil.sdcardPath + "LightSpot/0/icon/light_spot_icon_0_16_1.png", "", ""));


                break;
            case CarType.xts:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_0_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_0_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_1_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_1_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_2_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_2_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_3_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_3_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_4_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_4_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_5_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_5_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_6_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_6_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_7_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_7_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_8_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_8_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_9_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_9_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_10_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_10_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_11_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_11_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_12_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_12_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_13_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_13_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_14_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_14_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_15_0.png", PathUtil.sdcardPath + "LightSpot/1/icon/light_spot_icon_1_15_1.png", "", ""));


                break;
            case CarType.ct6:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_0_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_0_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_1_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_1_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_2_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_2_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_3_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_3_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_4_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_4_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_5_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_5_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_6_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_6_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_7_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_7_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_8_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_8_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_9_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_9_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_10_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_10_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_11_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_11_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_12_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_12_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_13_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_13_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_14_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_14_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_15_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_15_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_16_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_16_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_17_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_17_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_18_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_18_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_19_0.png", PathUtil.sdcardPath + "LightSpot/2/icon/light_spot_icon_2_19_1.png", "", ""));


                break;
            case CarType.xt5:
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_0_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_0_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_1_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_1_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_2_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_2_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_3_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_3_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_4_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_4_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_5_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_5_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_6_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_6_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_7_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_7_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_8_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_8_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_9_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_9_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_10_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_10_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_11_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_11_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_12_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_12_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_13_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_13_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_14_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_14_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_15_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_15_1.png", "", ""));
                data.add(new SaleSubItemBean(PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_16_0.png", PathUtil.sdcardPath + "LightSpot/3/icon/light_spot_icon_3_16_1.png", "", ""));


                break;


        }
        return data;
    }

    /**
     * 根据车型配置获取条目
     *
     * @param carType
     * @param config
     * @return
     */
    public static List<SaleSubItemBean> getSpecLightSpotItem(int carType, CarType.Config config) {
        List<SaleSubItemBean> data = new ArrayList<>();
        List<SaleSubItemBean> all = getLightSpotItem(carType);

        int[] specLightSpotItemIndex = getSpecLightSpotItemIndex(carType, config);
        for (int i : specLightSpotItemIndex) {
            data.add(all.get(i));
        }
        return data;
    }


    public static int[] getSpecLightSpotItemIndex(int carType, CarType.Config config) {
        switch (carType) {

            case CarType.ats_l:

                switch (config.num) {
                    case 0:
                        int[] index0 = {1, 2, 9, 0, 3, 4, 6, 5, 7, 8, 10};
                        return index0;
                    case 1:
                        int[] index1 = {15, 9, 0, 6, 5, 13, 11, 10, 14, 12};
                        return index1;
                    case 2:
                        int[] index2 = {9, 0, 6, 5, 10, 14, 12, 16};
                        return index2;
                    case 3:
                        int[] index3 = {9, 0, 6, 5, 16, 10, 14, 12};
                        return index3;
                    case 4:
                        int[] index4 = {9, 0, 6, 5, 10, 12};
                        return index4;
                }


                break;
            case CarType.xts:

                switch (config.num) {
                    case 0:
                        int[] index0 = {0, 4, 2, 3, 1, 7, 10, 5, 6, 11, 9};
                        return index0;
                    case 1:
                        int[] index1 = {4, 2, 3, 7, 10, 12, 11, 13};
                        return index1;
                    case 2:
                        int[] index2 = {4, 2, 3, 7, 14, 11, 13};
                        return index2;
                }

                break;
            case CarType.ct6:

                switch (config.num) {
                    case 0:
                        int[] index0 = {5, 12, 15, 16, 2, 3, 4, 11, 13, 14, 6, 7, 8, 9, 10};
                        return index0;
                    case 1:
                        int[] index1 = {5, 15, 16, 2, 3, 4, 11, 17, 14, 6, 7, 8};
                        return index1;
                    case 2:
                        int[] index2 = {5, 15, 2, 16, 3, 4, 11, 13, 14, 6, 7};
                        return index2;
                    case 3:
                        int[] index3 = {5, 19, 2, 11, 13, 14, 6, 7, 8, 9};
                        return index3;
                    case 4:
                        int[] index4 = {5, 19, 2, 11, 13, 14, 6};
                        return index4;
                    case 5:
                        int[] index5 = {12, 15, 16, 2, 3, 4, 11, 17, 14, 6, 7, 8, 9, 10};
                        return index5;
                    case 6:
                        int[] index6 = {5, 15, 16, 2, 3, 4, 11, 17, 14};
                        return index6;
                    case 7:
                        int[] index7 = {5, 1, 2, 11, 17, 14, 6, 7};
                        return index7;
                    case 8:
                        int[] index8 = {5, 1, 2, 11, 13, 14, 6, 18};
                        return index8;
                    case 9:
                        int[] index9 = {5, 1, 2, 11, 13, 14, 6, 18};
                        return index9;
                }


                break;
            case CarType.xt5:

                switch (config.num) {
                    case 0:
                        int[] index0 = {12, 4, 6, 1, 3, 7, 2, 5, 8, 9, 10, 11};
                        return index0;
                    case 1:
                        int[] index1 = {13, 12, 4, 6, 1, 3, 7, 2, 5, 8, 14};
                        return index1;
                    case 2:
                        int[] index2 = {12, 4, 3, 2, 5, 15, 14};
                        return index2;
                    case 3:
                        int[] index3 = {13, 0, 4, 3, 7, 15, 14, 16};
                        return index3;
                    case 4:
                        int[] index4 = {13, 0, 3, 7, 15, 14, 9};
                        return index4;
                    case 5:
                        int[] index5 = {0, 3, 7, 1, 15, 14};
                        return index5;
                }

                break;

        }
        return null;
    }

}
