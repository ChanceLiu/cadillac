package com.mobile.cadillacsaler.entity.sale;


/**
 * Created by chanc on 2018/6/14.
 */

public class SaleItem  {
    public int icon1;
    public int icon2;
    public int id;

    public SaleItem(int icon1, int icon2, int id) {
        this.icon1 = icon1;
        this.icon2 = icon2;
        this.id = id;
    }

}
