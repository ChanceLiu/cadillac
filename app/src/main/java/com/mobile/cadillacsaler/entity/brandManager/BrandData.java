package com.mobile.cadillacsaler.entity.brandManager;

import com.mobile.cadillacsaler.entity.CarType;

import java.util.ArrayList;

/**
 * Created by chanc on 2018/6/7.
 */

public class BrandData {
    public static ArrayList<BrandImageBean> getVideoData(int carType) {
        ArrayList<BrandImageBean> data = new ArrayList<>();
        switch (carType) {
            case CarType.ats_l:
                //0
                data.add(new BrandVideoBean("BrandManner/brand_manner_video_0.png", "BrandManner/brand_manner_video_0.mp4",
                        "快快快快", "这款ATSL主打年轻和运动，产品卖点就一个字“快”!"));

                break;
            case CarType.xts:
                //1
                data.add(new BrandVideoBean("BrandManner/brand_manner_video_1.png", "BrandManner/brand_manner_video_1.mp4",
                        "瞬间打开新世界", "新美式格调轿车XTS介绍"));

                break;
            case CarType.ct6:
                //2
                data.add(new BrandVideoBean("BrandManner/brand_manner_video_2.png", "BrandManner/brand_manner_video_2.mp4",
                        "风范 让世界勇敢向前", "新豪华旗舰凯迪拉克CT6介绍"));
                break;
            case CarType.xt5:
                //3
                data.add(new BrandVideoBean("BrandManner/brand_manner_video_3.png", "BrandManner/brand_manner_video_3.mp4",
                        "创乐之城", "在创乐之城 与梦中的凯迪拉克共同驰骋"));
                break;

        }
        data.add(new BrandVideoBean("BrandManner/brand_manner_video.png", "BrandManner/brand_manner_video.mp4",
                "所有的伟大，源于一个勇敢的开始！", "新年心愿，与凯迪拉克一起追梦。"));

        return data;
    }

    public static ArrayList<BrandImageBean> getImageData(int carType) {


        ArrayList<BrandImageBean> data = new ArrayList<>();
        switch (carType) {
            case CarType.ats_l:
                //0
                data.add(new BrandImageBean("BrandManner/brand_manner_image_0.png", "", "新豪华运动轿车ATS-L", "凯迪拉克ATSL图鉴"));

                break;
            case CarType.xts:
                //1
                data.add(new BrandImageBean("BrandManner/brand_manner_image_1.png", "", "全新一代凯迪拉克XTS", "凯迪拉克XTS图鉴"));

                break;
            case CarType.ct6:
                //2
                data.add(new BrandImageBean("BrandManner/brand_manner_image_2.png", "", "新豪华旗舰凯迪拉克CT6", "凯迪拉克CT6图鉴"));
                break;
            case CarType.xt5:
                //3
                data.add(new BrandImageBean("BrandManner/brand_manner_image_3.png", "", "新豪华都会SUV凯迪拉克XT5", "凯迪拉克XT5图鉴"));
                break;

        }
        data.add(new BrandImageBean("BrandManner/brand_manner_image.png", "", "Cadillac全家福", "凯迪拉克全系车型登场亮相"));

        return data;
    }


}
