package com.mobile.cadillacsaler.entity.sale;

import java.util.List;
import java.util.Map;

/**
 * Created by chanc on 2018/6/8.
 */

public class SkillDataBean {
    public String bigImage;
    public Map<String, String> iconsAndContents;
    public List<String> keys;

    public String getItemImage(String icon) {
        return iconsAndContents.get(icon);
    }
}
