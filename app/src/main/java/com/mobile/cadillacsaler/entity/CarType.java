package com.mobile.cadillacsaler.entity;

import com.mobile.cadillacsaler.R;
import com.mobile.cadillacsaler.entity.sale.Config;
import com.mobile.cadillacsaler.util.PathUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chanc on 2018/6/7.
 */

public class CarType {
    public static int currentType = 0;
    public static final int ats_l = 0;
    public static final int xts = 1;
    public static final int ct6 = 2;
    public static final int xt5 = 3;
    public static Map<Integer, Integer> carTypeIcon = new HashMap<>();

    {
        initType();
    }

    private static void initType() {
        carTypeIcon.put(CarType.ats_l, R.mipmap.car_type_small_0);
        carTypeIcon.put(CarType.xts, R.mipmap.car_type_small_1);
        carTypeIcon.put(CarType.ct6, R.mipmap.car_type_small_2);
        carTypeIcon.put(CarType.xt5, R.mipmap.car_type_small_3);
    }

    public static int getCarTypeIcon() {
        if (carTypeIcon.size() == 0)
            initType();

        return carTypeIcon.get(currentType);
    }

    /*
    根据车型获取配置
     */
    public static List<Config> getCartConfig(int carType) {
        List<Config> configs = new ArrayList<>();
        switch (carType) {
            case ats_l:


                configs.add(new Config(0,PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_0_0.png", PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_0_1.png"));
                configs.add(new Config(1,PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_1_0.png", PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_1_1.png"));
                configs.add(new Config(2,PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_2_0.png", PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_2_1.png"));
                configs.add(new Config(3,PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_3_0.png", PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_3_1.png"));
                configs.add(new Config(4,PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_4_0.png", PathUtil.sdcardPath + "LightSpot/0/config/light_spot_config_0_4_1.png"));

                break;
            case xts:
                configs.add(new Config(0,PathUtil.sdcardPath + "LightSpot/1/config/light_spot_config_1_0_0.png", PathUtil.sdcardPath + "LightSpot/1/config/light_spot_config_1_0_1.png"));
                configs.add(new Config(1,PathUtil.sdcardPath + "LightSpot/1/config/light_spot_config_1_1_0.png", PathUtil.sdcardPath + "LightSpot/1/config/light_spot_config_1_1_1.png"));
                configs.add(new Config(2,PathUtil.sdcardPath + "LightSpot/1/config/light_spot_config_1_2_0.png", PathUtil.sdcardPath + "LightSpot/1/config/light_spot_config_1_2_1.png"));


                break;
            case ct6:
                configs.add(new Config(0,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_0_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_0_1.png"));
                configs.add(new Config(1,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_1_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_1_1.png"));
                configs.add(new Config(2,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_2_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_2_1.png"));
                configs.add(new Config(3,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_3_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_3_1.png"));
                configs.add(new Config(4,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_4_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_4_1.png"));
                configs.add(new Config(5,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_5_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_5_1.png"));
                configs.add(new Config(6,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_6_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_6_1.png"));
                configs.add(new Config(7,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_7_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_7_1.png"));
                configs.add(new Config(8,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_8_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_8_1.png"));
                configs.add(new Config(9,PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_9_0.png", PathUtil.sdcardPath + "LightSpot/2/config/light_spot_config_2_9_1.png"));


                break;
            case xt5:
                configs.add(new Config(0,PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_0_0.png", PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_0_1.png"));
                configs.add(new Config(1,PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_1_0.png", PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_1_1.png"));
                configs.add(new Config(2,PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_2_0.png", PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_2_1.png"));
                configs.add(new Config(3,PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_3_0.png", PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_3_1.png"));
                configs.add(new Config(4,PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_4_0.png", PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_4_1.png"));
                configs.add(new Config(5,PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_5_0.png", PathUtil.sdcardPath + "LightSpot/3/config/light_spot_config_3_5_1.png"));

                break;
        }

        return configs;
    }


    public static class Config {
        public int num;
        public String icon1;
        public String icon2;
        public boolean isSelected;

        public Config(int num,String icon1, String icon2) {
            this.num=num;
            this.icon1 = icon1;
            this.icon2 = icon2;
        }
    }
}
