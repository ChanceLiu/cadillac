package com.mobile.cadillacsaler.entity.sale;

import com.mobile.cadillacsaler.entity.CarType;
import com.mobile.cadillacsaler.util.PathUtil;

import java.io.File;

/**
 * Created by chanc on 2018/6/8.
 */

public class DriveStepData {
    public static File getImageFile(int carType) {
        File file = null;
        switch (carType) {
            case CarType.ats_l:
                file = new File(PathUtil.sdcardPath+"Drive/drive_step_0.jpg");   break;
            case CarType.xts:
                file = new File(PathUtil.sdcardPath+"Drive/drive_step_1.jpg");   break;
            case CarType.ct6:
                file = new File(PathUtil.sdcardPath+"Drive/drive_step_2.jpg");   break;
            case CarType.xt5:
                file = new File(PathUtil.sdcardPath+"Drive/drive_step_3.jpg");   break;
        }
        return file;
    }

}
